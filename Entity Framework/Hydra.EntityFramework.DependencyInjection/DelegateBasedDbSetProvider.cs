﻿using Microsoft.EntityFrameworkCore;

namespace Hydra.EntityFramework.DependencyInjection;

/// <summary>Represents provider for <see cref="DbSet{TEntity}"/> used in Hydra components initialization.</summary>
/// <typeparam name="TResource">The type of resource in <see cref="DbSet{TEntity}"/>.</typeparam>
public class DelegateBasedDbSetProvider<TResource> : IDbSetProvider<TResource>
    where TResource : class
{
    private readonly Func<DbSet<TResource>> _getDbSet;

    /// <summary>Initializes a new instance of <see cref="DelegateBasedDbSetProvider{TResource}"/> with specified delegates for getting <see cref="DbContext"/> and extracting <see cref="DbSet{TEntity}"/> out of it.</summary>
    /// <exception cref="ArgumentNullException"><paramref name="getDbSet"/> is <see langword="null"/>.</exception>
    public DelegateBasedDbSetProvider(Func<DbSet<TResource>> getDbSet)
    {
        _getDbSet = getDbSet ?? throw new ArgumentNullException(nameof(getDbSet));
    }
    
    /// <summary>Gets <see cref="DbSet{TEntity}"/> for Hydra components initialization.</summary>
    /// <returns>The <see cref="DbSet{TEntity}"/> object.</returns>
    public DbSet<TResource> Get()
    {
        return _getDbSet();
    }
}