﻿using Hydra.Abstractions;
using Hydra.DependencyInjection;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace Hydra.EntityFramework.DependencyInjection;

/// <summary>Represents feature for adding EntityFramework-based Hydra components in <see cref="IServiceCollection"/>.</summary>
/// <typeparam name="TResource">The type of resource to be managed with components from feature.</typeparam>
public class EntityFrameworkFeature<TResource> : IEntityFrameworkFeature<TResource> where TResource : class
{
    /// <summary>Initializes a new instance of <see cref="EntityFrameworkFeature{TResource}"/> with specified <see cref="IServiceCollection"/> and delegate for getting <see cref="DbSet{TEntity}"/> from <see cref="IServiceCollection"/>.</summary>
    /// <param name="services">The <see cref="IServiceCollection"/> for Hydra components to be added in.</param>
    /// <param name="getDbSet">The delegate for getting <see cref="DbSet{TResource}"/> from <see cref="IServiceProvider"/>.</param>
    /// <exception cref="ArgumentNullException"><paramref name="services"/> or <paramref name="getDbSet"/> is <see langword="null"/>.</exception>
    public EntityFrameworkFeature(IServiceCollection services, Func<IServiceProvider, DbSet<TResource>> getDbSet)
    {
        Services = services ?? throw new ArgumentNullException(nameof(services));
        Services.AddScoped<IDbSetProvider<TResource>>(sp => new DelegateBasedDbSetProvider<TResource>(() => getDbSet(sp)));
    }

    /// <summary>Gets <see cref="IServiceCollection"/> in which feature adds Hydra components.</summary>
    /// <returns>The <see cref="IServiceCollection"/> in which feature adds Hydra components.</returns>
    public IServiceCollection Services { get; }

    /// <summary>Adds <see cref="DbSetBasedCreator{TResource}"/> in the <see cref="IServiceCollection"/>.</summary>
    /// <param name="configure">The delegate for configuring <see cref="DbSetBasedCreator{TResource}"/>.</param>
    /// <returns>The component for extending <see cref="DbSetBasedCreator{TResource}"/> as component of the other type.</returns>
    public ExtensionBuilder<DbSetBasedCreator<TResource>> Create(
        Func<IServiceProvider, DbSetBasedCreator<TResource>, ICreator<TResource>>? configure = null)
    {
        Services.AddScoped<DbSetBasedCreator<TResource>>(sp =>
            new DbSetBasedCreator<TResource>(sp.GetRequiredService<IDbSetProvider<TResource>>().Get()));
        Services.AddScoped<ICreator<TResource>>(sp =>
        {
            var component = sp.GetRequiredService<DbSetBasedCreator<TResource>>();
            return configure?.Invoke(sp, component) ?? component;
        });

        return new ExtensionBuilder<DbSetBasedCreator<TResource>>(Services);
    }

    /// <summary>Adds <see cref="DbSetBasedModifier{TResource}"/> in the <see cref="IServiceCollection"/>.</summary>
    /// <param name="configure">The delegate for configuring <see cref="DbSetBasedModifier{TResource}"/>.</param>
    /// <returns>The component for extending <see cref="DbSetBasedModifier{TResource}"/> as component of the other type.</returns>
    public ExtensionBuilder<DbSetBasedModifier<TResource>> Modify(
        Func<IServiceProvider, DbSetBasedModifier<TResource>, IModifier<TResource>>? configure = null)
    {
        Services.AddScoped<DbSetBasedModifier<TResource>>(sp =>
            new DbSetBasedModifier<TResource>(sp.GetRequiredService<IDbSetProvider<TResource>>().Get()));
        Services.AddScoped<IModifier<TResource>>(sp =>
        {
            var component = sp.GetRequiredService<DbSetBasedModifier<TResource>>();
            return configure?.Invoke(sp, component) ?? component;
        });

        return new ExtensionBuilder<DbSetBasedModifier<TResource>>(Services);
    }

    /// <summary>Adds <see cref="DbSetBasedRemover{TResource}"/> in the <see cref="IServiceCollection"/>.</summary>
    /// <param name="configure">The delegate for configuring <see cref="DbSetBasedRemover{TResource}"/>.</param>
    /// <returns>The component for extending <see cref="DbSetBasedRemover{TResource}"/> as component of the other type.</returns>
    public ExtensionBuilder<DbSetBasedRemover<TResource>> Remove(
        Func<IServiceProvider, DbSetBasedRemover<TResource>, IRemover<TResource>>? configure = null)
    {
        Services.AddScoped<DbSetBasedRemover<TResource>>(sp =>
            new DbSetBasedRemover<TResource>(sp.GetRequiredService<IDbSetProvider<TResource>>().Get()));
        Services.AddScoped<IRemover<TResource>>(sp =>
        {
            var component = sp.GetRequiredService<DbSetBasedRemover<TResource>>();
            return configure?.Invoke(sp, component) ?? component;
        });

        return new ExtensionBuilder<DbSetBasedRemover<TResource>>(Services);
    }

    /// <summary>Adds <see cref="DbSetBasedSearcher{TResource,TCriteria}"/> in the <see cref="IServiceCollection"/>.</summary>
    /// <param name="specification">The specification for resource filtering.</param>
    /// <param name="configure">The delegate for configuring <see cref="DbSetBasedSearcher{TResource,TCriteria}"/>.</param>
    /// <typeparam name="TCriteria">The type of criteria to be used for accessing the resource matching <paramref name="specification"/>.</typeparam>
    /// <exception cref="ArgumentNullException"><paramref name="specification"/> is <see langword="null"/>.</exception>
    /// <returns>The component for extending <see cref="DbSetBasedSearcher{TResource,TCriteria}"/> as component of the other type.</returns>
    public ExtensionBuilder<DbSetBasedSearcher<TResource, TCriteria>> SearchBy<TCriteria>(
        ISpecification<TResource, TCriteria> specification,
        Func<IServiceProvider, DbSetBasedSearcher<TResource, TCriteria>, ISearcher<TResource?, TCriteria>>? configure = null)
    {
        ArgumentNullException.ThrowIfNull(specification);
        
        Services.AddScoped<DbSetBasedSearcher<TResource, TCriteria>>(sp =>
            new DbSetBasedSearcher<TResource, TCriteria>(
                sp.GetRequiredService<IDbSetProvider<TResource>>().Get(), specification));
        Services.AddScoped<ISearcher<TResource?, TCriteria>>(sp =>
        {
            var component = sp.GetRequiredService<DbSetBasedSearcher<TResource, TCriteria>>();
            return configure?.Invoke(sp, component) ?? component;
        });

        return new ExtensionBuilder<DbSetBasedSearcher<TResource, TCriteria>>(Services);
    }

    /// <summary>Adds <see cref="DbSetBasedSequenceProvider{TResource,TCriteria}"/> in the <see cref="IServiceCollection"/>.</summary>
    /// <param name="specification">The specification for resource filtering.</param>
    /// <param name="configure">The delegate for configuring <see cref="DbSetBasedSequenceProvider{TResource,TCriteria}"/>.</param>
    /// <typeparam name="TCriteria">The type of criteria to be used for accessing sequence of the resources matching <paramref name="specification"/>.</typeparam>
    /// <exception cref="ArgumentNullException"><paramref name="specification"/> is <see langword="null"/>.</exception>
    /// <returns>The component for extending <see cref="DbSetBasedSequenceProvider{TResource,TCriteria}"/> as component of the other type.</returns>
    public ExtensionBuilder<DbSetBasedSequenceProvider<TResource, TCriteria>> SearchInSequenceBy<TCriteria>(
        ISpecification<TResource, TCriteria> specification,
        Func<IServiceProvider, DbSetBasedSequenceProvider<TResource, TCriteria>, ISequenceProvider<TResource, TCriteria>>? configure = null)
    {
        ArgumentNullException.ThrowIfNull(specification);
        
        Services.AddScoped<DbSetBasedSequenceProvider<TResource, TCriteria>>(sp =>
            new DbSetBasedSequenceProvider<TResource, TCriteria>(
                sp.GetRequiredService<IDbSetProvider<TResource>>().Get(), specification));
        Services.AddScoped<ISequenceProvider<TResource, TCriteria>>(
            sp =>
            {
                var component = sp.GetRequiredService<DbSetBasedSequenceProvider<TResource, TCriteria>>();
                return configure?.Invoke(sp, component) ?? component;
            });

        return new ExtensionBuilder<DbSetBasedSequenceProvider<TResource, TCriteria>>(Services);
    }
}