﻿using Hydra.DependencyInjection;
using Microsoft.EntityFrameworkCore;

namespace Hydra.EntityFramework.DependencyInjection.Extensions;

public static class ResourceManagementSetupExtensions 
{
    /// <summary>Defines a <see cref="EntityFrameworkFeature{TResource}"/> for resource to be managed with.</summary>
    /// <param name="getDbSet">The delegate for getting <see cref="DbSet{TResource}"/> from <see cref="IServiceProvider"/>.</param>
    /// <param name="configureFeature">The delegate for feature configuring.</param>
    /// <param name="setup">The <see cref="ResourceManagementSetup{TResource}"/> object for configuring resource management with.</param>
    /// <exception cref="ArgumentNullException">Any of <paramref name="setup"/>, <paramref name="getDbSet"/>, <paramref name="configureFeature"/> is <see langword="null"/>.</exception>
    /// <returns>The <see cref="ResourceManagementSetup{TResource}"/> object for configuring resource management.</returns>
    public static ResourceManagementSetup<TResource> IsManagedByEntityFramework<TResource>(
        this ResourceManagementSetup<TResource> setup,
        Func<IServiceProvider, DbSet<TResource>> getDbSet,
        Action<IEntityFrameworkFeature<TResource>> configureFeature)
        where TResource : class
    {
        ArgumentNullException.ThrowIfNull(setup);
        ArgumentNullException.ThrowIfNull(getDbSet);
        ArgumentNullException.ThrowIfNull(configureFeature);

        return setup.IsManagedBy(sc => new EntityFrameworkFeature<TResource>(sc, getDbSet),
            configureFeature);
    }
}