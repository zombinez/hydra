﻿using Microsoft.EntityFrameworkCore;

namespace Hydra.EntityFramework.DependencyInjection;

/// <summary>Represents provider for <see cref="DbSet{TEntity}"/> used in Hydra components initialization.</summary>
/// <typeparam name="TResource">The type of resource in <see cref="DbSet{TEntity}"/>.</typeparam>
public interface IDbSetProvider<TResource> where TResource : class
{
    /// <summary>Gets <see cref="DbSet{TEntity}"/> for Hydra components initialization.</summary>
    /// <returns>The <see cref="DbSet{TEntity}"/> object.</returns>
    DbSet<TResource> Get();
}