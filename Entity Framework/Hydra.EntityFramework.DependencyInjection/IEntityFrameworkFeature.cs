﻿using Hydra.Abstractions;
using Hydra.DependencyInjection;
using Microsoft.Extensions.DependencyInjection;

namespace Hydra.EntityFramework.DependencyInjection;

/// <summary>Represents feature for adding EntityFramework-based Hydra components in <see cref="IServiceCollection"/>.</summary>
/// <typeparam name="TResource">The type of resource to be managed with components from feature.</typeparam>
public interface IEntityFrameworkFeature<TResource> where TResource : class
{
    /// <summary>Gets <see cref="IServiceCollection"/> in which feature adds Hydra components.</summary>
    /// <returns>The <see cref="IServiceCollection"/> in which feature adds Hydra components.</returns>
    public IServiceCollection Services { get; }

    /// <summary>Adds <see cref="DbSetBasedCreator{TResource}"/> in the <see cref="IServiceCollection"/>.</summary>
    /// <param name="configure">The delegate for configuring <see cref="DbSetBasedCreator{TResource}"/>.</param>
    /// <returns>The component for extending <see cref="DbSetBasedCreator{TResource}"/> as component of the other type.</returns>
    ExtensionBuilder<DbSetBasedCreator<TResource>> Create(
        Func<IServiceProvider, DbSetBasedCreator<TResource>, ICreator<TResource>>? configure = null);

    /// <summary>Adds <see cref="DbSetBasedModifier{TResource}"/> in the <see cref="IServiceCollection"/>.</summary>
    /// <param name="configure">The delegate for configuring <see cref="DbSetBasedModifier{TResource}"/>.</param>
    /// <returns>The component for extending <see cref="DbSetBasedModifier{TResource}"/> as component of the other type.</returns>
    ExtensionBuilder<DbSetBasedModifier<TResource>> Modify(
        Func<IServiceProvider, DbSetBasedModifier<TResource>, IModifier<TResource>>? configure = null);

    /// <summary>Adds <see cref="DbSetBasedRemover{TResource}"/> in the <see cref="IServiceCollection"/>.</summary>
    /// <param name="configure">The delegate for configuring <see cref="DbSetBasedRemover{TResource}"/>.</param>
    /// <returns>The component for extending <see cref="DbSetBasedRemover{TResource}"/> as component of the other type.</returns>
    ExtensionBuilder<DbSetBasedRemover<TResource>> Remove(
        Func<IServiceProvider, DbSetBasedRemover<TResource>, IRemover<TResource>>? configure = null);

    /// <summary>Adds <see cref="DbSetBasedSearcher{TResource,TCriteria}"/> in the <see cref="IServiceCollection"/>.</summary>
    /// <param name="specification">The specification for resource filtering.</param>
    /// <param name="configure">The delegate for configuring <see cref="DbSetBasedSearcher{TResource,TCriteria}"/>.</param>
    /// <typeparam name="TCriteria">The type of criteria to be used for accessing the resource matching <paramref name="specification"/>.</typeparam>
    /// <exception cref="ArgumentNullException"><paramref name="specification"/> is <see langword="null"/>.</exception>
    /// <returns>The component for extending <see cref="DbSetBasedSearcher{TResource,TCriteria}"/> as component of the other type.</returns>
    ExtensionBuilder<DbSetBasedSearcher<TResource, TCriteria>> SearchBy<TCriteria>(
        ISpecification<TResource, TCriteria> specification, 
        Func<IServiceProvider, DbSetBasedSearcher<TResource, TCriteria>, ISearcher<TResource?, TCriteria>>? configure = null);

    /// <summary>Adds <see cref="DbSetBasedSequenceProvider{TResource,TCriteria}"/> in the <see cref="IServiceCollection"/>.</summary>
    /// <param name="specification">The specification for resource filtering.</param>
    /// <param name="configure">The delegate for configuring <see cref="DbSetBasedSequenceProvider{TResource,TCriteria}"/>.</param>
    /// <typeparam name="TCriteria">The type of criteria to be used for accessing sequence of the resources matching <paramref name="specification"/>.</typeparam>
    /// <exception cref="ArgumentNullException"><paramref name="specification"/> is <see langword="null"/>.</exception>
    /// <returns>The component for extending <see cref="DbSetBasedSequenceProvider{TResource,TCriteria}"/> as component of the other type.</returns>
    ExtensionBuilder<DbSetBasedSequenceProvider<TResource, TCriteria>> SearchInSequenceBy<TCriteria>(
        ISpecification<TResource, TCriteria> specification, 
        Func<IServiceProvider, DbSetBasedSequenceProvider<TResource, TCriteria>, ISequenceProvider<TResource, TCriteria>>? configure = null);
}