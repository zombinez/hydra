﻿using Hydra.Abstractions;
using Microsoft.EntityFrameworkCore;

namespace Hydra.EntityFramework;

/// <summary>Represents <see cref="DbSet{TEntity}"/>-based component for saving modified resource.</summary>
/// <typeparam name="TResource">The type of the resource to be modified. Must be reference type.</typeparam>
public class DbSetBasedModifier<TResource> : IModifier<TResource> where TResource : class
{
    private readonly DbSet<TResource> _storage;

    /// <summary>Initializes a new instance of <see cref="DbSetBasedModifier{TResource}"/> with specified <see cref="DbSet{TEntity}"/> as storage.</summary>
    /// <param name="storage">The <see cref="DbSet{TEntity}"/> object to use as storage.</param>
    /// <exception cref="ArgumentNullException"><paramref name="storage"/> is <see langword="null"/>.</exception>
    public DbSetBasedModifier(DbSet<TResource> storage)
    {
        _storage = storage ?? throw new ArgumentNullException(nameof(storage));
    }

    /// <summary>Saves modified resource resource.</summary>
    /// <param name="resource">The resource to be saved.</param>
    /// <exception cref="ArgumentNullException"><paramref name="resource"/> instance is <see langword="null"/>.</exception>
    /// <returns>The task object representing the asynchronous operation.</returns>
    public Task ModifyAsync(TResource resource)
    {
        ArgumentNullException.ThrowIfNull(resource);

        _storage.Update(resource);
        
        return Task.CompletedTask;
    }
}