﻿using Hydra.Abstractions;
using Microsoft.EntityFrameworkCore;

namespace Hydra.EntityFramework;

/// <summary>Represents <see cref="DbSet{TEntity}"/>-based component for removing resource.</summary>
/// <typeparam name="TResource">The type of resource to be removed. Must be reference type.</typeparam>
public class DbSetBasedRemover<TResource> : IRemover<TResource> where TResource : class
{
    private readonly DbSet<TResource> _storage;

    /// <summary>Initializes a new instance of <see cref="DbSetBasedRemover{TResource}"/> with specified <see cref="DbSet{TEntity}"/> as storage.</summary>
    /// <param name="storage">The <see cref="DbSet{TEntity}"/> object to use as storage.</param>
    /// <exception cref="ArgumentNullException"><paramref name="storage"/> is <see langword="null"/>.</exception>
    public DbSetBasedRemover(DbSet<TResource> storage)
    {
        _storage = storage ?? throw new ArgumentNullException(nameof(storage));
    }

    /// <summary>Removes resource.</summary>
    /// <param name="resource">The resource to be removed.</param>
    /// <exception cref="ArgumentNullException"><paramref name="resource"/> instance is <see langword="null"/>.</exception>
    /// <returns>The task object representing the asynchronous operation.</returns>
    public Task RemoveAsync(TResource resource)
    {
        ArgumentNullException.ThrowIfNull(resource);

        _storage.Remove(resource);

        return Task.CompletedTask;
    }
}