﻿using Hydra.Abstractions;
using Hydra.EntityFramework.Extensions;
using Microsoft.EntityFrameworkCore;

namespace Hydra.EntityFramework;

/// <summary>Represents component for searching resource matching specified criteria.</summary>
/// <typeparam name="TResource">The type of the resource to be found.</typeparam>
/// <typeparam name="TCriteria">The type of criteria for resource to be found by.</typeparam>
public class DbSetBasedSearcher<TResource, TCriteria> : ISearcher<TResource?, TCriteria> where TResource : class
{
    private readonly DbSet<TResource> _resources;
    private readonly ISpecification<TResource, TCriteria> _specification;

    /// <summary>Initializes a new instance of <see cref="DbSetBasedSearcher{TResource,TCriteria}"/> with specified <see cref="ISpecification{TResource,TCriteria}"/> and <see cref="DbSet{TEntity}"/> as storage.</summary>
    /// <param name="resources">The <see cref="DbSet{TEntity}"/> object to use as storage.</param>
    /// <param name="specification">The specification for <see cref="DbSet{TEntity}"/> to be applied to.</param>
    /// <exception cref="ArgumentNullException"><paramref name="resources"/> or <paramref name="specification"/> is <see langword="null"/>.</exception>
    public DbSetBasedSearcher(DbSet<TResource> resources, ISpecification<TResource, TCriteria> specification)
    {
        _resources = resources ?? throw new ArgumentNullException(nameof(resources));
        _specification = specification ?? throw new ArgumentNullException(nameof(specification));
    }

    /// <summary>Returns resource that matches given criteria.</summary>
    /// <param name="criteria">The criteria to search resource by.</param>
    /// <param name="cancellationToken">A <see cref="CancellationToken"/> to observe while waiting for the task to complete.</param>
    /// <exception cref="ArgumentNullException"><paramref name="criteria" /> is <see langword="null" />.</exception>
    /// <returns>The task object representing the asynchronous operation.</returns>
    public Task<TResource?> SearchAsync(TCriteria criteria, CancellationToken cancellationToken = default)
    {
        ArgumentNullException.ThrowIfNull(criteria);
        
        return _resources.Matching(_specification, criteria).FirstOrDefaultAsync(cancellationToken);
    }
}