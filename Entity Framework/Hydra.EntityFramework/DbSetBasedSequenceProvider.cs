﻿using Hydra.Abstractions;
using Hydra.EntityFramework.Extensions;
using Microsoft.EntityFrameworkCore;

namespace Hydra.EntityFramework;

/// <summary>Represents a provider for a sequence of resources matching specified criteria.</summary>
/// <typeparam name="TResource">The type of resources in sequence.</typeparam>
/// <typeparam name="TCriteria">The type of criteria to select resources by.</typeparam>
public class DbSetBasedSequenceProvider<TResource, TCriteria> : ISequenceProvider<TResource, TCriteria>
    where TResource : class
{
    private readonly DbSet<TResource> _resources;
    private readonly ISpecification<TResource, TCriteria> _specification;

    /// <summary>Initializes a new instance of <see cref="DbSetBasedSequenceProvider{TResource,TCriteria}"/> with specified <see cref="ISpecification{TResource,TCriteria}"/> and <see cref="DbSet{TEntity}"/> as storage.</summary>
    /// <param name="resources">The <see cref="DbSet{TEntity}"/> object to use as storage.</param>
    /// <param name="specification">The specification for <see cref="DbSet{TEntity}"/> to be applied to.</param>
    /// <exception cref="ArgumentNullException"><paramref name="resources"/> or <paramref name="specification"/> is <see langword="null"/>.</exception>
    public DbSetBasedSequenceProvider(DbSet<TResource> resources, ISpecification<TResource, TCriteria> specification)
    {
        _resources = resources ?? throw new ArgumentNullException(nameof(resources));
        _specification = specification ?? throw new ArgumentNullException(nameof(specification));
    }

    /// <summary>Gets sequence of resources matching specified criteria.</summary>
    /// <param name="criteria">The criteria to select resources by.</param>
    /// <exception cref="ArgumentNullException"><paramref name="criteria"/> is <see langword="null" />.</exception>
    /// <returns>Sequence of resources matching certain criteria.</returns>
    public IAsyncEnumerable<TResource> Get(TCriteria criteria)
    {
        ArgumentNullException.ThrowIfNull(criteria);
        
        return _resources.Matching(_specification, criteria).AsAsyncEnumerable();
    }
}