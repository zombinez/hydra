﻿using Microsoft.EntityFrameworkCore;

namespace Hydra.EntityFramework.Extensions;

public static class DbSetExtensions
{
    /// <summary>Filters resources in <see cref="DbSet{TEntity}"/> by applying <see cref="ISpecification{TResource,TCriteria}"/> to it.</summary>
    /// <param name="storage">The <see cref="DbSet{TEntity}"/> object containing resources to be filtered.</param>
    /// <param name="specification">The specification for <see cref="DbSet{TEntity}"/> to be applied to.</param>
    /// <param name="criteria">The criteria for resources from <see cref="DbSet{TEntity}"/> to be filtered by.</param>
    /// <typeparam name="TResource">The type of resource from <see cref="DbSet{TEntity}"/> to be filtered.</typeparam>
    /// <typeparam name="TCriteria">The type of criteria for resources in <see cref="DbSet{TEntity}"/> to be filtered by.</typeparam>
    /// <exception cref="ArgumentNullException"><paramref name="storage"/>, <paramref name="specification"/> or <paramref name="criteria"/> is <see langword="null"/>.</exception>
    /// <returns>The <see cref="IQueryable{T}"/> containing resources matching specified criteria.</returns>
    public static IQueryable<TResource> Matching<TResource, TCriteria>(this DbSet<TResource> storage, ISpecification<TResource, TCriteria> specification, TCriteria criteria)
        where TResource : class
    {
        ArgumentNullException.ThrowIfNull(storage);
        ArgumentNullException.ThrowIfNull(criteria);
        ArgumentNullException.ThrowIfNull(specification);

        return specification.Apply(storage, criteria);
    }
}