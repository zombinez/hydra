﻿using Microsoft.EntityFrameworkCore;

namespace Hydra.EntityFramework;

/// <summary>Represents a component for applying filtration to <see cref="DbSet{TEntity}"/> based on specified criteria.</summary>
/// <typeparam name="TResource">The type of resource in <see cref="DbSet{TEntity}"/> to be filtered.</typeparam>
/// <typeparam name="TCriteria">The type of criteria for resources in <see cref="DbSet{TEntity}"/> to be filtered by.</typeparam>
public interface ISpecification<TResource, in TCriteria> where TResource : class
{
    /// <summary>Applies specification to <see cref="DbSet{TEntity}"/> based on specified criteria.</summary>
    /// <param name="storage">The <see cref="DbSet{TEntity}"/> for specification to be applied to.</param>
    /// <param name="criteria">The criteria for resources in <see cref="DbSet{TEntity}"/> to be filtered by.</param>
    /// <exception cref="ArgumentNullException"><paramref name="storage"/> or <paramref name="criteria"/> is <see langword="null"/>.</exception>
    /// <returns>The <see cref="IQueryable{T}"/> containing resources matching specified criteria.</returns>
    IQueryable<TResource> Apply(DbSet<TResource> storage, TCriteria criteria);
}