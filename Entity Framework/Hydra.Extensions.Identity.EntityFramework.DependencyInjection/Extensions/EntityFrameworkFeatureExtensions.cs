﻿using Hydra.Abstractions;
using Hydra.DependencyInjection;
using Hydra.EntityFramework.DependencyInjection;
using Hydra.Extensions.Identity.Abstractions;
using Microsoft.Extensions.DependencyInjection;

namespace Hydra.Extensions.Identity.EntityFramework.DependencyInjection.Extensions;

public static class EntityFrameworkFeatureExtensions
{
    /// <summary>Adds <see cref="DbSetBasedSearcherById{TResource,TId}"/> in the <see cref="IServiceCollection"/> as <see cref="ISearcherById{TResource,TId}"/>.</summary>
    /// <param name="feature">The feature used to manage resource of type <typeparamref name="TResource"/>.</param>
    /// <param name="configure">The delegate for configuring <see cref="DbSetBasedSearcherById{TResource,TId}"/>.</param>
    /// <exception cref="ArgumentNullException"><paramref name="feature"/> is <see langword="null"/>.</exception>
    /// <returns>The component for extending <see cref="DbSetBasedSearcherById{TResource,TId}"/> as component of the other type.</returns>
    public static ExtensionBuilder<DbSetBasedSearcherById<TResource, TId>> SearchById<TResource, TId>(
        this IEntityFrameworkFeature<TResource> feature,
        Func<IServiceProvider, DbSetBasedSearcherById<TResource, TId>, ISearcherById<TResource, TId>>? configure = null)
        where TResource : class, IIdOwner<TId>
        where TId : notnull
    {
        ArgumentNullException.ThrowIfNull(feature);

        feature.Services.AddScoped<DbSetBasedSearcherById<TResource, TId>>(sp =>
            new DbSetBasedSearcherById<TResource, TId>(sp.GetRequiredService<IDbSetProvider<TResource>>().Get()));
        feature.Services.AddScoped<ISearcherById<TResource, TId>>(sp =>
        {
            var component = sp.GetRequiredService<DbSetBasedSearcherById<TResource, TId>>();
            return configure?.Invoke(sp, component) ?? component;
        });
        feature.Services.AddScoped<ISearcher<TResource?, TId>>(sp =>
            sp.GetRequiredService<ISearcherById<TResource, TId>>());

        return new ExtensionBuilder<DbSetBasedSearcherById<TResource, TId>>(feature.Services);
    }

    /// <summary>Adds <see cref="DbSetBasedBatchSearcherById{TResource,TId}"/> in the <see cref="IServiceCollection"/> as <see cref="IBatchSearcherById{TResource,TId}"/>.</summary>
    /// <param name="feature">The feature used to manage resource of type <typeparamref name="TResource"/>.</param>
    /// <param name="configure">The delegate for configuring <see cref="DbSetBasedBatchSearcherById{TResource,TId}"/>.</param>
    /// <exception cref="ArgumentNullException"><paramref name="feature"/> is <see langword="null"/>.</exception>
    /// <returns>The component for extending <see cref="DbSetBasedBatchSearcherById{TResource,TId}"/> as component of the other type.</returns>
    public static ExtensionBuilder<DbSetBasedBatchSearcherById<TResource, TId>> SearchInBatchesByIds<TResource, TId>(
        this IEntityFrameworkFeature<TResource> feature,
        Func<IServiceProvider, DbSetBasedBatchSearcherById<TResource, TId>, IBatchSearcherById<TResource, TId>>? configure = null)
        where TResource : class, IIdOwner<TId> 
        where TId : notnull
    {
        ArgumentNullException.ThrowIfNull(feature);

        feature.Services.AddScoped<DbSetBasedBatchSearcherById<TResource, TId>>(sp =>
            new DbSetBasedBatchSearcherById<TResource, TId>(
                sp.GetRequiredService<IDbSetProvider<TResource>>().Get()));
        feature.Services.AddScoped<IBatchSearcherById<TResource, TId>>(sp =>
        {
            var component = sp.GetRequiredService<DbSetBasedBatchSearcherById<TResource, TId>>();
            return configure?.Invoke(sp, component) ?? component;
        });
        feature.Services.AddScoped<ISearcher<Batch<TResource, TId>, IReadOnlySet<TId>>>(sp =>
            sp.GetRequiredService<IBatchSearcherById<TResource, TId>>());

        return new ExtensionBuilder<DbSetBasedBatchSearcherById<TResource, TId>>(feature.Services);
    }
}