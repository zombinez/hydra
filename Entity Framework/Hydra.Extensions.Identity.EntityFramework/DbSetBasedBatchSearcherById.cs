﻿using Hydra.Extensions.Identity.Abstractions;
using Microsoft.EntityFrameworkCore;

namespace Hydra.Extensions.Identity.EntityFramework;

/// <summary>Represents a <see cref="DbSet{TEntity}"/>-based component for finding batch of resources with ids from specified set.</summary>
/// <typeparam name="TResource">The type of resource to be found. Must implement <see cref="IIdOwner{TId}"/>.</typeparam>
/// <typeparam name="TId">The type of id of the resource.</typeparam>
public class DbSetBasedBatchSearcherById<TResource, TId> : IBatchSearcherById<TResource, TId>
    where TResource : class, IIdOwner<TId>
    where TId : notnull
{
    private readonly DbSet<TResource> _storage;

    /// <summary>Initializes a new instance of <see cref="DbSetBasedBatchSearcherById{TResource,TId}"/> with specified <see cref="DbSet{TEntity}"/> as storage.</summary>
    /// <param name="storage">The <see cref="DbSet{TEntity}"/> object to use as storage.</param>
    /// <exception cref="ArgumentNullException"><paramref name="storage"/> is <see langword="null"/>.</exception>
    public DbSetBasedBatchSearcherById(DbSet<TResource> storage)
    {
        _storage = storage ?? throw new ArgumentNullException(nameof(storage));
    }

    /// <summary>Returns batch of resources with ids from specified set.</summary>
    /// <param name="ids">The ids set to search resources by.</param>
    /// <param name="cancellationToken">A <see cref="CancellationToken"/> to observe while waiting for the task to complete.</param>
    /// <exception cref="ArgumentNullException"><paramref name="ids"/> is <see langword="null"/>.</exception>
    /// <returns>The task object representing the asynchronous operation.</returns>
    public async Task<Batch<TResource, TId>> SearchAsync(IReadOnlySet<TId> ids, CancellationToken cancellationToken = default)
    {
        ArgumentNullException.ThrowIfNull(ids);

        if (ids.Count == 0)
            return new Batch<TResource, TId>(Enumerable.Empty<TResource>(), Enumerable.Empty<TId>());

        var resources = await _storage.Where(r => ids.Contains(r.Id)).ToArrayAsync(cancellationToken);
        var missingIds = ids.Except(resources.Select(r => r.Id));

        return new Batch<TResource, TId>(resources, missingIds);
    }
}