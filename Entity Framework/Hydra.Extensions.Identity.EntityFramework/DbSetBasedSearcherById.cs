﻿using Hydra.Extensions.Identity.Abstractions;
using Microsoft.EntityFrameworkCore;

namespace Hydra.Extensions.Identity.EntityFramework;

/// <summary>Represents a <see cref="DbSet{TEntity}"/>-based component for finding resource with specified id.</summary>
/// <typeparam name="TResource">The type of resource to be found. Must implement <see cref="IIdOwner{TId}"/>.</typeparam>
/// <typeparam name="TId">The type of id of the resource.</typeparam>
public class DbSetBasedSearcherById<TResource, TId> : ISearcherById<TResource, TId>
    where TResource : class, IIdOwner<TId>
    where TId : notnull
{
    private readonly DbSet<TResource> _storage;

    /// <summary>Initializes a new instance of <see cref="DbSetBasedSearcherById{TResource,TId}"/> with specified <see cref="DbSet{TEntity}"/> as storage.</summary>
    /// <param name="storage">The <see cref="DbSet{TEntity}"/> object to use as storage.</param>
    /// <exception cref="ArgumentNullException"><paramref name="storage"/> is <see langword="null"/>.</exception>
    public DbSetBasedSearcherById(DbSet<TResource> storage)
    {
        _storage = storage ?? throw new ArgumentNullException(nameof(storage));
    }

    /// <summary>Returns resource with specified id.</summary>
    /// <param name="id">The id to search resource by.</param>
    /// <param name="cancellationToken">A <see cref="CancellationToken"/> to observe while waiting for the task to complete.</param>
    /// <exception cref="ArgumentNullException"><paramref name="id"/> is <see langword="null"/>.</exception>
    /// <returns>The task object representing the asynchronous operation.</returns>
    public Task<TResource?> SearchAsync(TId id, CancellationToken cancellationToken = default)
    {
        ArgumentNullException.ThrowIfNull(id);

        return _storage.Where(r => r.Id.Equals(id)).FirstOrDefaultAsync(cancellationToken);
    }
}