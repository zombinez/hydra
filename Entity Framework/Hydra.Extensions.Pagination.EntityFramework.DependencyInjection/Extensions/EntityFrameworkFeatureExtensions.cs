﻿using Hydra.Abstractions;
using Hydra.DependencyInjection;
using Hydra.EntityFramework;
using Hydra.EntityFramework.DependencyInjection;
using Hydra.Extensions.Pagination.Abstractions;
using Microsoft.Extensions.DependencyInjection;

namespace Hydra.Extensions.Pagination.EntityFramework.DependencyInjection.Extensions;

public static class EntityFrameworkFeatureExtensions
{
    /// <summary>Adds <see cref="DbSetBasedPageSearcher{TResource,TCriteria}"/> in the <see cref="IServiceCollection"/>.</summary>
    /// <param name="feature">The feature used to manage resource of type <typeparamref name="TResource"/>.</param>
    /// <param name="specification">The specification for resource filtering.</param>
    /// <param name="configure">The delegate for configuring <see cref="DbSetBasedPageSearcher{TResource,TCriteria}"/>.</param>
    /// <typeparam name="TCriteria">The type of criteria to be used for accessing the resource matching <paramref name="specification"/>.</typeparam>
    /// <typeparam name="TResource">The type of resource to be searched in pages.</typeparam>
    /// <exception cref="ArgumentNullException"><paramref name="specification"/> is <see langword="null"/>.</exception>
    /// <returns>The component for extending <see cref="DbSetBasedPageSearcher{TResource,TCriteria}"/> as component of the other type.</returns>
    public static ExtensionBuilder<DbSetBasedPageSearcher<TResource, TCriteria>> SearchInPagesBy<TResource, TCriteria>(
        this IEntityFrameworkFeature<TResource> feature,
        ISpecification<TResource, TCriteria> specification,
        Func<IServiceProvider, DbSetBasedPageSearcher<TResource, TCriteria>, IPageSearcher<TResource, TCriteria>>? configure = null)
        where TResource : class
    {
        ArgumentNullException.ThrowIfNull(feature);

        feature.Services.AddScoped<DbSetBasedPageSearcher<TResource, TCriteria>>(sp =>
            new DbSetBasedPageSearcher<TResource, TCriteria>(sp.GetRequiredService<IDbSetProvider<TResource>>().Get(), specification));
        feature.Services.AddScoped<IPageSearcher<TResource, TCriteria>>(sp =>
        {
            var component = sp.GetRequiredService<DbSetBasedPageSearcher<TResource, TCriteria>>();
            return configure?.Invoke(sp, component) ?? component;
        });
        feature.Services.AddScoped<ISearcher<Page<TResource>, PageSpecification<TCriteria>>>(sp =>
            sp.GetRequiredService<IPageSearcher<TResource, TCriteria>>());

        return new ExtensionBuilder<DbSetBasedPageSearcher<TResource, TCriteria>>(feature.Services);
    }
}