﻿using Hydra.EntityFramework;
using Hydra.EntityFramework.Extensions;
using Hydra.Extensions.Pagination.Abstractions;
using Microsoft.EntityFrameworkCore;

namespace Hydra.Extensions.Pagination.EntityFramework;

/// <summary>Represents component for finding selection of resources matching certain criteria by pages in storage.</summary>
/// <typeparam name="TResource">The type of the resource to be found.</typeparam>
/// <typeparam name="TCriteria">The type of criteria for resource to be found by.</typeparam>
public class DbSetBasedPageSearcher<TResource, TCriteria> : IPageSearcher<TResource, TCriteria>
    where TResource : class
{
    private readonly DbSet<TResource> _resources;
    private readonly ISpecification<TResource, TCriteria> _specification;

    /// <summary>Initializes a new instance of <see cref="DbSetBasedPageSearcher{TResource,TCriteria}"/> with specified <see cref="ISpecification{TResource,TCriteria}"/> and <see cref="DbSet{TEntity}"/> as storage.</summary>
    /// <param name="resources">The <see cref="DbSet{TEntity}"/> object to use as storage.</param>
    /// <param name="specification">The specification for <see cref="DbSet{TEntity}"/> to be applied to.</param>
    /// <exception cref="ArgumentNullException"><paramref name="resources"/> or <paramref name="specification"/> is <see langword="null"/>.</exception>
    public DbSetBasedPageSearcher(DbSet<TResource> resources, ISpecification<TResource, TCriteria> specification)
    {
        _resources = resources ?? throw new ArgumentNullException(nameof(resources));
        _specification = specification ?? throw new ArgumentNullException(nameof(specification));
    }

    /// <summary>Returns page of resources matching specified criteria.</summary>
    /// <param name="pageSpecification">Specification for page extraction.</param>
    /// <param name="cancellationToken">A <see cref="CancellationToken"/> to observe while waiting for the task to complete.</param>
    /// <exception cref="ArgumentNullException"><paramref name="pageSpecification"/> is <see langword="null"/>.</exception>
    /// <returns>The task object representing the asynchronous operation.</returns>
    public async Task<Page<TResource>> SearchAsync(PageSpecification<TCriteria> pageSpecification, CancellationToken cancellationToken = default)
    {
        ArgumentNullException.ThrowIfNull(pageSpecification);

        var resources = await _resources
            .Matching(_specification, pageSpecification.Criteria)
            .Skip(pageSpecification.Offset)
            .Take(pageSpecification.Capacity)
            .ToArrayAsync(cancellationToken);

        return new Page<TResource>(resources);
    }
}