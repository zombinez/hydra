﻿using Hydra.Abstractions;
using Hydra.Examples.ShoppingList.API.Controllers.Customers.DTOs;
using Hydra.Examples.ShoppingList.API.Criterias.Customer;
using Hydra.Examples.ShoppingList.API.Models;
using Hydra.Examples.ShoppingList.API.Persistence;
using Hydra.Extensions.Identity.Abstractions;
using Microsoft.AspNetCore.Mvc;

namespace Hydra.Examples.ShoppingList.API.Controllers.Customers;

[ApiController]
[Route("customers")]
public class CustomersController : ControllerBase
{
    private readonly ICreator<Customer> _customerCreator;
    private readonly ISearcher<Customer?, Which.HasLoginAndPassword> _customerSearcherByLoginAndPassword;
    private readonly ISearcher<Customer?, Which.HasLogin> _customerSearcherByLogin;
    private readonly ISearcherById<Customer, Guid> _customerSearcherById;
    private readonly ICreator<Models.ShoppingList> _shoppingListCreator;
    private readonly ITransaction _transaction;

    public CustomersController(
        ISearcher<Customer?, Which.HasLoginAndPassword> customerSearcherByLoginAndPassword,
        ICreator<Customer> customerCreator,
        ISearcher<Customer?, Which.HasLogin> customerSearcherByLogin,
        ISearcherById<Customer, Guid> customerSearcherById,
        ICreator<Models.ShoppingList> shoppingListCreator,
        ITransaction transaction)
    {
        _customerSearcherByLoginAndPassword = customerSearcherByLoginAndPassword ?? throw new ArgumentNullException(nameof(customerSearcherByLoginAndPassword));
        _customerCreator = customerCreator ?? throw new ArgumentNullException(nameof(customerCreator));
        _shoppingListCreator = shoppingListCreator ?? throw new ArgumentNullException(nameof(shoppingListCreator));
        _customerSearcherById = customerSearcherById ?? throw new ArgumentNullException(nameof(customerSearcherById));
        _customerSearcherByLogin = customerSearcherByLogin ?? throw new ArgumentNullException(nameof(customerSearcherByLogin));
        _transaction = transaction ?? throw new ArgumentNullException(nameof(transaction));
    }

    [HttpPost]
    public async Task<ActionResult<CustomerResponseBody>> Create([FromBody] CustomerCreationRequestBody body)
    {
        var customerWithSameLogin = await _customerSearcherByLogin.SearchAsync(new Which.HasLogin(body.Login));
        if (customerWithSameLogin is not null)
            return BadRequest("Customer with same login already exists.");
        
        var customer = Customer.New(body.Login, body.Password);
        
        await _customerCreator.CreateAsync(customer);
        await _transaction.CommitAsync();
        
        return Created($"customers/{customer.Id}", new CustomerCreationResponseBody()
        {
            Id = customer.Id
        });
    }

    [HttpGet]
    public async Task<ActionResult<CustomerResponseBody>> Get([FromQuery] string login, [FromHeader] string password)
    {
        var customer = await _customerSearcherByLoginAndPassword.SearchAsync(new Which.HasLoginAndPassword(login, password));
        
        if (customer is null)
            return NotFound();

        return Ok(new CustomerResponseBody()
        {
            Id = customer.Id,
            Login = customer.Login
        });
    }

    [HttpPost("{customerId:guid}/shopping-lists")]
    public async Task<ActionResult<ShoppingListCreationRequestBody>> CreateShoppingList(
        [FromBody] ShoppingListCreationRequestBody body,
        [FromRoute] Guid customerId)
    {
        var customer = await _customerSearcherById.SearchAsync(customerId);
        if (customer is null)
            return BadRequest("Customer does not exist.");

        var shoppingList = customer.CreateShoppingList(body.Title);
        await _shoppingListCreator.CreateAsync(shoppingList);
        await _transaction.CommitAsync();

        return Created($"shopping-lists/{shoppingList.Id}", new ShoppingListCreationResponseBody()
        {
            Id = shoppingList.Id
        });
    }
}