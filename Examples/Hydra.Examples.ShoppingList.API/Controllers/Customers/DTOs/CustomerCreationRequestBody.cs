﻿namespace Hydra.Examples.ShoppingList.API.Controllers.Customers.DTOs;

public class CustomerCreationRequestBody
{
    public string Login { get; set; } = string.Empty;

    public string Password { get; set; } = string.Empty;
}