﻿namespace Hydra.Examples.ShoppingList.API.Controllers.Customers.DTOs;

public class CustomerCreationResponseBody
{
    public Guid Id { get; set; }
}