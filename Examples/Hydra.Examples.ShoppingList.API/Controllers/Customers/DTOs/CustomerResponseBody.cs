﻿namespace Hydra.Examples.ShoppingList.API.Controllers.Customers.DTOs;

public class CustomerResponseBody
{
    public Guid Id { get; set; }
    
    public string Login { get; set; } = string.Empty;
}