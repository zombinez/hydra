﻿namespace Hydra.Examples.ShoppingList.API.Controllers.Customers.DTOs;

public class ShoppingListCreationRequestBody
{
    public string Title { get; set; } = string.Empty;
}