﻿namespace Hydra.Examples.ShoppingList.API.Controllers.Customers.DTOs;

public class ShoppingListCreationResponseBody
{
    public Guid Id { get; set; }
}