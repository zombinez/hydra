﻿namespace Hydra.Examples.ShoppingList.API.Controllers.Products.DTOs;

public class ProductCreationRequestBody
{
    public string Name { get; set; } = string.Empty;
    
    public double PriceDollars { get; set; }
}