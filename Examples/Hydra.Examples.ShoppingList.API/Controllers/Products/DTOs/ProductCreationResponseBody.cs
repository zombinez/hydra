﻿namespace Hydra.Examples.ShoppingList.API.Controllers.Products.DTOs;

public class ProductCreationResponseBody
{
    public Guid Id { get; set; }
}