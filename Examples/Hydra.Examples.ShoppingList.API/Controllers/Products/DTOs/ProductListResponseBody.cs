﻿namespace Hydra.Examples.ShoppingList.API.Controllers.Products.DTOs;

public class ProductListResponseBody
{
    public Product[] Products { get; set; } = [];
    
    public int TotalCount { get; set; }
    
    public class Product
    {
        public Guid Id { get; set; }

        public string Name { get; set; } = string.Empty;
        
        public double PriceDollars { get; set; }
    }
}