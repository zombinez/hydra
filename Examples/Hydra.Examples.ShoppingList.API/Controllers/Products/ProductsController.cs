﻿using Hydra.Abstractions;
using Hydra.Examples.ShoppingList.API.Controllers.Products.DTOs;
using Hydra.Examples.ShoppingList.API.Criterias;
using Hydra.Examples.ShoppingList.API.Criterias.ShoppingList;
using Hydra.Examples.ShoppingList.API.Models;
using Hydra.Examples.ShoppingList.API.Persistence;
using Hydra.Extensions.Identity.Abstractions;
using Hydra.Extensions.Pagination.Abstractions;
using Hydra.Extensions.Pagination.Abstractions.Extensions;
using Microsoft.AspNetCore.Mvc;

namespace Hydra.Examples.ShoppingList.API.Controllers.Products;

[ApiController]
[Route("products")]
public class ProductsController : ControllerBase
{
    private readonly ICreator<Product> _productCreator;
    private readonly IPageSearcher<Product, All> _productPageSearcher;
    private readonly ISearcherById<Product, Guid> _productSearcherById;
    private readonly IModifier<Product> _productModifier;
    private readonly ISequenceProvider<Models.ShoppingList, Which.ContainsPositionWithProduct>
        _shoppingListSequenceProviderByProduct;

    private readonly IModifier<Models.ShoppingList> _shoppingListModifier;
    private readonly ITransaction _transaction;

    public ProductsController(ICreator<Product> productCreator,
        IPageSearcher<Product, All> productPageSearcher,
        ISearcherById<Product, Guid> productSearcherById,
        IModifier<Product> productModifier,
        ISequenceProvider<Models.ShoppingList, Which.ContainsPositionWithProduct> shoppingListSequenceProviderByProduct,
        IModifier<Models.ShoppingList> shoppingListModifier,
        ITransaction transaction)
    {
        _productCreator = productCreator ?? throw new ArgumentNullException(nameof(productCreator));
        _productPageSearcher = productPageSearcher ?? throw new ArgumentNullException(nameof(productPageSearcher));
        _productSearcherById = productSearcherById ?? throw new ArgumentNullException(nameof(productSearcherById));
        _productModifier = productModifier ?? throw new ArgumentNullException(nameof(productModifier));
        _shoppingListSequenceProviderByProduct = shoppingListSequenceProviderByProduct ?? throw new ArgumentNullException(nameof(shoppingListSequenceProviderByProduct));
        _shoppingListModifier = shoppingListModifier ?? throw new ArgumentNullException(nameof(shoppingListModifier));
        _transaction = transaction ?? throw new ArgumentNullException(nameof(transaction));
    }

    [HttpPost]
    public async Task<ActionResult<ProductCreationResponseBody>> Create([FromBody] ProductCreationRequestBody body)
    {
        var product = Product.New(body.Name, body.PriceDollars);

        await _productCreator.CreateAsync(product);
        await _transaction.CommitAsync();

        return Created($"products/{product.Id}", new ProductCreationResponseBody()
        {
            Id = product.Id
        });
    }

    [HttpGet]
    public async Task<ActionResult<ProductListResponseBody>> List([FromQuery] int offset = 0, [FromQuery] int capacity = 100)
    {
        var page = await _productPageSearcher.SearchAsync(new All(), capacity, offset, isWithTotalCount: true);

        return Ok(new ProductListResponseBody()
        {
            Products = page.Select(p => new ProductListResponseBody.Product()
            {
                Id = p.Id,
                Name = p.Name,
                PriceDollars = p.PriceDollars
            }).ToArray(),
            TotalCount = page.TotalCount!.Value
        });
    }

    [HttpPatch("{productId:guid}")]
    public async Task<IActionResult> Update([FromRoute] Guid productId, [FromBody] ProductUpdateRequestBody body)
    {
        var product = await _productSearcherById.SearchAsync(productId);
        if (product is null)
            return BadRequest("Product does not exist.");

        product.Name = body.Name;
        product.PriceDollars = body.PriceDollars;

        await _productModifier.ModifyAsync(product);
        await _transaction.CommitAsync();

        return NoContent();
    }

    [HttpDelete("{productId:guid}")]
    public async Task<IActionResult> Remove([FromRoute] Guid productId)
    {
        var product = await _productSearcherById.SearchAsync(productId);
        if (product is null)
            return BadRequest("Product does not exist.");

        var shoppingListsWithProduct =
            _shoppingListSequenceProviderByProduct.Get(new Which.ContainsPositionWithProduct(product.Id));

        await foreach (var shoppingList in shoppingListsWithProduct)
        {
            var positionWithItem = shoppingList.Positions.First(p => p.ProductId == product.Id);
            shoppingList.RemovePosition(positionWithItem, out var wasRemoved);
            if (wasRemoved)
                await _shoppingListModifier.ModifyAsync(shoppingList);
        }
        await _transaction.CommitAsync();
        
        return NoContent();
    }
}