﻿namespace Hydra.Examples.ShoppingList.API.Controllers.ShoppingLists.DTOs;

public class SearchShoppingListsResponseBody
{
    public ShoppingList[] ShoppingLists { get; set; } = [];
    
    public int TotalCount { get; set; }
    
    public class ShoppingList
    {
        public Guid Id { get; set; }
    
        public Guid CustomerId { get; set; }

        public string Title { get; set; } = string.Empty;

        public ShoppingListPositionDto[] Positions { get; set; } = [];
    }
}