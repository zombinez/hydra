﻿namespace Hydra.Examples.ShoppingList.API.Controllers.ShoppingLists.DTOs;

public class ShoppingListAddOrUpdatePositionsRequestBody
{
    public ShoppingListPosition[] Positions { get; set; } = [];
    
    public class ShoppingListPosition
    {
        public Guid ProductId { get; set; }
        public int Quantity { get; set; }
    }
}