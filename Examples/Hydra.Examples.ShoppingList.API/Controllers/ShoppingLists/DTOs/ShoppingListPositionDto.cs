﻿namespace Hydra.Examples.ShoppingList.API.Controllers.ShoppingLists.DTOs;

public class ShoppingListPositionDto
{
    public Guid ProductId { get; set; }
        
    public int Quantity { get; set; }
        
    public double TotalPriceDollars { get; set; }
}