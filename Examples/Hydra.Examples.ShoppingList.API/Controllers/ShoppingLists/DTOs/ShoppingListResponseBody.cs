﻿namespace Hydra.Examples.ShoppingList.API.Controllers.ShoppingLists.DTOs;

public class ShoppingListResponseBody
{
    public Guid Id { get; set; }
    
    public Guid CustomerId { get; set; }

    public string Title { get; set; } = string.Empty;

    public ShoppingListPositionDto[] Positions { get; set; } = [];
}