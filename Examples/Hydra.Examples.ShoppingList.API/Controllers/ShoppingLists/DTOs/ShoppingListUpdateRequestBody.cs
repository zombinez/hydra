﻿namespace Hydra.Examples.ShoppingList.API.Controllers.ShoppingLists.DTOs;

public class ShoppingListUpdateRequestBody
{
    public string Title { get; set; } = string.Empty;
}