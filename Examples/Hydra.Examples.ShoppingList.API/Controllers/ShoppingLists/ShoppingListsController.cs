﻿using Hydra.Abstractions;
using Hydra.Examples.ShoppingList.API.Controllers.ShoppingLists.DTOs;
using Hydra.Examples.ShoppingList.API.Criterias.ShoppingList;
using Hydra.Examples.ShoppingList.API.Models;
using Hydra.Examples.ShoppingList.API.Models.Extensions;
using Hydra.Examples.ShoppingList.API.Persistence;
using Hydra.Extensions.Identity.Abstractions;
using Hydra.Extensions.Pagination.Abstractions;
using Hydra.Extensions.Pagination.Abstractions.Extensions;
using Microsoft.AspNetCore.Mvc;

namespace Hydra.Examples.ShoppingList.API.Controllers.ShoppingLists;

[ApiController]
[Route("shopping-lists")]
public class ShoppingListsController : ControllerBase
{
    private readonly ISearcherById<Models.ShoppingList, Guid> _shoppintListSearcherById;
    private readonly IModifier<Models.ShoppingList> _shoppingListModifier;
    private readonly IBatchSearcherById<Product, Guid> _productBatchSearcherById;
    private readonly IRemover<Models.ShoppingList> _shoppingListRemover;
    private readonly IPageSearcher<Models.ShoppingList, Which.BelongToCustomer> _shoppingListSearcherByCustomer;
    private readonly ITransaction _transaction;

    public ShoppingListsController(
        ISearcherById<Models.ShoppingList, Guid> shoppintListSearcherById,
        IModifier<Models.ShoppingList> shoppingListModifier,
        IBatchSearcherById<Product, Guid> productBatchSearcherById,
        IRemover<Models.ShoppingList> shoppingListRemover,
        IPageSearcher<Models.ShoppingList, Which.BelongToCustomer> shoppingListSearcherByCustomer,
        ITransaction transaction)
    {
        _shoppintListSearcherById = shoppintListSearcherById ?? throw new ArgumentNullException(nameof(shoppintListSearcherById));
        _shoppingListModifier = shoppingListModifier ?? throw new ArgumentNullException(nameof(shoppingListModifier));
        _productBatchSearcherById = productBatchSearcherById ?? throw new ArgumentNullException(nameof(productBatchSearcherById));
        _shoppingListRemover = shoppingListRemover ?? throw new ArgumentNullException(nameof(shoppingListRemover));
        _shoppingListSearcherByCustomer = shoppingListSearcherByCustomer ?? throw new ArgumentNullException(nameof(shoppingListSearcherByCustomer));
        _transaction = transaction ?? throw new ArgumentNullException(nameof(transaction));
    }

    [HttpGet]
    public async Task<ActionResult<SearchShoppingListsResponseBody>> SearchByPages(
        [FromQuery] Guid customerId,
        [FromQuery] int offset = 0,
        [FromQuery] int capacity = 5)
    {
        var page = await _shoppingListSearcherByCustomer.SearchAsync(new Which.BelongToCustomer(customerId), capacity, offset, isWithTotalCount: true);

        return Ok(new SearchShoppingListsResponseBody()
        {
            ShoppingLists = page.Select(l => l.ToSearchResponseItem()).ToArray(),
            TotalCount = page.TotalCount!.Value
        });
    }
    
    [HttpGet("{shoppingListId:guid}")]
    public async Task<ActionResult<ShoppingListResponseBody>> Get([FromRoute] Guid shoppingListId)
    {
        var shoppingList = await _shoppintListSearcherById.SearchAsync(shoppingListId);
        if (shoppingList is null)
            return NotFound("Shopping list does not exist.");

        return Ok(shoppingList.ToResponseBody());
    }
    
    [HttpPut("{shoppingListId:guid}/positions")]
    public async Task<IActionResult> AddOrUpdatePositions([FromRoute] Guid shoppingListId, ShoppingListAddOrUpdatePositionsRequestBody body)
    {
        var shoppingList = await _shoppintListSearcherById.SearchAsync(shoppingListId);
        if (shoppingList is null)
            return NotFound("Shopping list does not exist.");

        var quantityPerProductId = body.Positions.ToDictionary(p => p.ProductId, p => p.Quantity);
        var productIds = quantityPerProductId.Keys.ToHashSet();
        var products = await _productBatchSearcherById.SearchAsync(productIds);
        if (products.MissingIds.Count > 0)
            return BadRequest("Some products do not exist.");

        foreach (var product in products)
            shoppingList.AddOrUpdatePosition(product, quantityPerProductId[product.Id]);

        await _shoppingListModifier.ModifyAsync(shoppingList);
        await _transaction.CommitAsync();

        return NoContent();
    }
    
    [HttpPatch("{shoppingListId:guid}")]
    public async Task<IActionResult> Update([FromRoute] Guid shoppingListId,
        [FromBody] ShoppingListUpdateRequestBody body)
    {
        var shoppingList = await _shoppintListSearcherById.SearchAsync(shoppingListId);
        if (shoppingList is null)
            return BadRequest("Shopping list does not exist.");

        shoppingList.Title = body.Title;
        await _shoppingListModifier.ModifyAsync(shoppingList);
        await _transaction.CommitAsync();

        return NoContent();
    }

    [HttpDelete("{shoppingListId:guid}")]
    public async Task<IActionResult> Remove([FromRoute] Guid shoppingListId)
    {
        var shoppingList = await _shoppintListSearcherById.SearchAsync(shoppingListId);
        if (shoppingList is null)
            return BadRequest("Shopping list does not exist.");

        await _shoppingListRemover.RemoveAsync(shoppingList);
        await _transaction.CommitAsync();

        return NoContent();
    }
}