﻿namespace Hydra.Examples.ShoppingList.API.Criterias.Customer;

public static class Which
{
    public record HasLoginAndPassword(string Login, string Password);

    public record HasLogin(string Login);
}