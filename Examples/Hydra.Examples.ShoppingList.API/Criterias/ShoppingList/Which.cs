﻿namespace Hydra.Examples.ShoppingList.API.Criterias.ShoppingList;

public static class Which
{
    public record BelongToCustomer(Guid CustomerId);

    public record ContainsPositionWithProduct(Guid ProductId);
}