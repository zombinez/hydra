﻿using Hydra.Extensions.Identity.Abstractions;
// ReSharper disable EntityFramework.ModelValidation.UnlimitedStringLength
#pragma warning disable CS8618 // Non-nullable field must contain a non-null value when exiting constructor. Consider declaring as nullable.

namespace Hydra.Examples.ShoppingList.API.Models;

public class Customer : IIdOwner<Guid>
{
    private string _login;
    private string _password;

    private Customer(Guid id, string login, string password)
    {
        Id = id;
        Login = login;
        Password = password;
    }
    
    public Guid Id { get; private set; }

    public string Login
    {
        get => _login;
        private set
        {
            ArgumentException.ThrowIfNullOrWhiteSpace(value, nameof(Login));
            _login = value.Trim();
        }
    }

    public string Password
    {
        get => _password;
        private set
        {
            ArgumentException.ThrowIfNullOrWhiteSpace(value, nameof(Password));
            _password = value.Trim();
        }
    }

    public ShoppingList CreateShoppingList(string title)
    {
        return ShoppingList.New(this, title);
    }

    public static Customer New(string login, string password)
    {
        return new Customer(Guid.NewGuid(), login, password);
    }
}