﻿using Hydra.Examples.ShoppingList.API.Controllers.ShoppingLists.DTOs;

namespace Hydra.Examples.ShoppingList.API.Models.Extensions;

public static class ShoppingListExtensions
{
    public static ShoppingListResponseBody ToResponseBody(this ShoppingList list)
    {
        ArgumentNullException.ThrowIfNull(list);

        return new ShoppingListResponseBody()
        {
            Id = list.Id,
            CustomerId = list.CustomerId,
            Title = list.Title,
            Positions = list.Positions.Select(p => new ShoppingListPositionDto()
            {
                ProductId = p.ProductId,
                Quantity = p.Quantity,
                TotalPriceDollars = p.TotalPriceDollars
            }).ToArray()
        };
    }
    
    public static SearchShoppingListsResponseBody.ShoppingList ToSearchResponseItem(this ShoppingList list)
    {
        ArgumentNullException.ThrowIfNull(list);

        return new SearchShoppingListsResponseBody.ShoppingList()
        {
            Id = list.Id,
            CustomerId = list.CustomerId,
            Title = list.Title,
            Positions = list.Positions.Select(p => new ShoppingListPositionDto()
            {
                ProductId = p.ProductId,
                Quantity = p.Quantity,
                TotalPriceDollars = p.TotalPriceDollars
            }).ToArray()
        };
    }
}