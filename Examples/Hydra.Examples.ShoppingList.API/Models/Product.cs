﻿using Hydra.Extensions.Identity.Abstractions;
// ReSharper disable EntityFramework.ModelValidation.UnlimitedStringLength
#pragma warning disable CS8618 // Non-nullable field must contain a non-null value when exiting constructor. Consider declaring as nullable.

namespace Hydra.Examples.ShoppingList.API.Models;

public class Product : IIdOwner<Guid>
{
    private string _name;
    private double _priceDollars;
    
    private Product(Guid id, string name, double priceDollars)
    {
        Id = id;
        Name = name;
        PriceDollars = priceDollars;
    }
    
    public Guid Id { get; private set; }

    public string Name
    {
        get => _name;
        set
        {
            ArgumentException.ThrowIfNullOrWhiteSpace(value, nameof(Name));
            _name = value.Trim();
        }
    }

    public double PriceDollars
    {
        get => _priceDollars;
        set
        {
            ArgumentOutOfRangeException.ThrowIfNegativeOrZero(value, nameof(PriceDollars));
            _priceDollars = value;
        }
    }

    public static Product New(string name, double priceDollars)
    {
        return new Product(Guid.NewGuid(), name, priceDollars);
    }
}