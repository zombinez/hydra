﻿using Hydra.Extensions.Identity.Abstractions;
// ReSharper disable EntityFramework.ModelValidation.UnlimitedStringLength
// ReSharper disable NonReadonlyMemberInGetHashCode
// ReSharper disable UnusedMember.Local
// ReSharper disable ConditionalAccessQualifierIsNonNullableAccordingToAPIContract
#pragma warning disable CS8618 // Non-nullable field must contain a non-null value when exiting constructor. Consider declaring as nullable.

namespace Hydra.Examples.ShoppingList.API.Models;

public class ShoppingList : IIdOwner<Guid>
{
    private string _title;
    private HashSet<ShoppingListPosition>? _positions;
    
    private ShoppingList(Guid id, Guid customerId, string title)
    {
        Id = id;
        CustomerId = customerId;
        Title = title;
    }
    
    public Guid Id { get; private set; }
    
    public Guid CustomerId { get; private set; }

    public string Title
    {
        get => _title;
        set
        {
            ArgumentException.ThrowIfNullOrWhiteSpace(value, nameof(_title));
            _title = value.Trim();
        }
    }

    public IReadOnlyCollection<ShoppingListPosition> Positions
    {
        get => _positions ??= [];
        private set => _positions = value?.ToHashSet();
    }
    
    private HashSet<ShoppingListPosition> PositionsSet => _positions ??= [];

    public void AddOrUpdatePosition(Product product, int quantity)
    {
        var position = ShoppingListPosition.New(this, product, quantity);

        PositionsSet.Remove(position);
        PositionsSet.Add(position);
    }

    public void RemovePosition(ShoppingListPosition position, out bool wasRemoved)
    {
        if (position.ShoppingListId != Id)
            throw new InvalidOperationException("Position from another shopping list could not be removed.");

        wasRemoved = PositionsSet.Remove(position);
    }

    public static ShoppingList New(Customer customer, string title)
    {
        ArgumentNullException.ThrowIfNull(customer);
        
        return new ShoppingList(Guid.NewGuid(), customer.Id, title);
    }

    public override bool Equals(object? obj)
    {
        return obj is ShoppingList other && other.Id == Id;
    }

    public override int GetHashCode()
    {
        return HashCode.Combine(GetType(), Id);
    }
}