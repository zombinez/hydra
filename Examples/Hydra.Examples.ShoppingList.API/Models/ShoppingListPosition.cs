﻿// ReSharper disable NonReadonlyMemberInGetHashCode
namespace Hydra.Examples.ShoppingList.API.Models;

public class ShoppingListPosition
{
    private int _quantity;

    private ShoppingListPosition(Guid shoppingListId, Guid productId, int quantity)
    {
        ShoppingListId = shoppingListId;
        ProductId = productId;
        Quantity = quantity;
    }
    
    public Guid ShoppingListId { get; private set; }
    
    public Guid ProductId { get; private set; }

    public int Quantity
    {
        get => _quantity;
        set
        {
            ArgumentOutOfRangeException.ThrowIfNegativeOrZero(value, nameof(Quantity));
            _quantity = value;
        }
    }

    public double TotalPriceDollars => Product.PriceDollars * Quantity;

    public Product Product { get; private set; } = null!;

    public static ShoppingListPosition New(ShoppingList list, Product product, int quantity)
    {
        ArgumentNullException.ThrowIfNull(list);
        ArgumentNullException.ThrowIfNull(product);
        
        var result = new ShoppingListPosition(list.Id, product.Id, quantity)
        {
            Product = product
        };

        return result;
    }

    public override bool Equals(object? obj)
    {
        return obj is ShoppingListPosition other && other.ShoppingListId == ShoppingListId && other.ProductId == ProductId;
    }

    protected bool Equals(ShoppingListPosition other)
    {
        return ShoppingListId.Equals(other.ShoppingListId) && ProductId.Equals(other.ProductId);
    }

    public override int GetHashCode()
    {
        return HashCode.Combine(ShoppingListId, ProductId);
    }
}