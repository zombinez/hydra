﻿using Hydra.Examples.ShoppingList.API.Models;
using Hydra.Examples.ShoppingList.API.Persistence.TypeConfigurations;
using Microsoft.EntityFrameworkCore;

namespace Hydra.Examples.ShoppingList.API.Persistence;

public class AppDbContext : DbContext
{
    public DbSet<ShoppingListPosition> ShoppingListPositions { get; set; }
    
    public DbSet<Customer> Customers { get; set; }
    
    public DbSet<Models.ShoppingList> ShoppingLists { get; set; }
    
    public DbSet<Product> Products { get; set; }
    
    public AppDbContext(DbContextOptions<AppDbContext> options) : base (options) { }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        base.OnModelCreating(modelBuilder);
        modelBuilder.ApplyConfiguration(new CustomerTypeConfiguration());
        modelBuilder.ApplyConfiguration(new ProductTypeConfiguration());
        modelBuilder.ApplyConfiguration(new ShoppingListTypeConfiguration());
        modelBuilder.ApplyConfiguration(new ShoppingListPositionTypeConfiguration());
        
    }
}