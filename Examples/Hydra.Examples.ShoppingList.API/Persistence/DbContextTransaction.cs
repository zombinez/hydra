﻿using Microsoft.EntityFrameworkCore;

namespace Hydra.Examples.ShoppingList.API.Persistence;

public class DbContextTransaction<TDbContext> : ITransaction where TDbContext : DbContext
{
    private readonly TDbContext _dbContext;

    public DbContextTransaction(TDbContext dbContext)
    {
        _dbContext = dbContext ?? throw new ArgumentNullException(nameof(dbContext));
    }

    public Task CommitAsync()
    {
        return _dbContext.SaveChangesAsync();
    }
}