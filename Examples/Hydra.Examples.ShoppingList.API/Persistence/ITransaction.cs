﻿namespace Hydra.Examples.ShoppingList.API.Persistence;

public interface ITransaction
{
    Task CommitAsync();
}