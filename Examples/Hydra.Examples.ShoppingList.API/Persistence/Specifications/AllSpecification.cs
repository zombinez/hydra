﻿using Hydra.EntityFramework;
using Hydra.Examples.ShoppingList.API.Criterias;
using Microsoft.EntityFrameworkCore;

namespace Hydra.Examples.ShoppingList.API.Persistence.Specifications;

public class AllSpecification<TResource> : ISpecification<TResource, All> where TResource : class
{
    public IQueryable<TResource> Apply(DbSet<TResource> storage, All criteria)
    {
        return storage;
    }
}