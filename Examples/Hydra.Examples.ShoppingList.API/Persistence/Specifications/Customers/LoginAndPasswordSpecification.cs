﻿using Hydra.EntityFramework;
using Hydra.Examples.ShoppingList.API.Criterias.Customer;
using Hydra.Examples.ShoppingList.API.Models;
using Microsoft.EntityFrameworkCore;

namespace Hydra.Examples.ShoppingList.API.Persistence.Specifications.Customers;

public class LoginAndPasswordSpecification : ISpecification<Customer, Which.HasLoginAndPassword>
{
    public IQueryable<Customer> Apply(DbSet<Customer> storage, Which.HasLoginAndPassword criteria)
    {
        ArgumentNullException.ThrowIfNull(criteria);

        return storage.Where(c => c.Login == criteria.Login && c.Password == criteria.Password);
    }
}