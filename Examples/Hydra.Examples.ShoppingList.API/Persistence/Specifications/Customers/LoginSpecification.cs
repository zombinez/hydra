﻿using Hydra.EntityFramework;
using Hydra.Examples.ShoppingList.API.Criterias.Customer;
using Hydra.Examples.ShoppingList.API.Models;
using Microsoft.EntityFrameworkCore;

namespace Hydra.Examples.ShoppingList.API.Persistence.Specifications.Customers;

public class LoginSpecification : ISpecification<Customer, Which.HasLogin>
{
    public IQueryable<Customer> Apply(DbSet<Customer> storage, Which.HasLogin criteria)
    {
        ArgumentNullException.ThrowIfNull(criteria);

        return storage.Where(c => c.Login == criteria.Login);
    }
}