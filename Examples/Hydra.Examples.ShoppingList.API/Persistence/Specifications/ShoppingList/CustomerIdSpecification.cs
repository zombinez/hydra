﻿using Hydra.EntityFramework;
using Hydra.Examples.ShoppingList.API.Criterias.ShoppingList;
using Microsoft.EntityFrameworkCore;

namespace Hydra.Examples.ShoppingList.API.Persistence.Specifications.ShoppingList;

public class CustomerIdSpecification : ISpecification<Models.ShoppingList, Which.BelongToCustomer>
{
    public IQueryable<Models.ShoppingList> Apply(DbSet<Models.ShoppingList> storage, Which.BelongToCustomer criteria)
    {
        ArgumentNullException.ThrowIfNull(storage);
        ArgumentNullException.ThrowIfNull(criteria);

        return storage.Where(sl => sl.CustomerId == criteria.CustomerId);
    }
}