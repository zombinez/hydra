﻿using Hydra.EntityFramework;
using Hydra.Examples.ShoppingList.API.Criterias.ShoppingList;
using Microsoft.EntityFrameworkCore;

namespace Hydra.Examples.ShoppingList.API.Persistence.Specifications.ShoppingList;

public class ProductInPositionSpecification : ISpecification<Models.ShoppingList, Which.ContainsPositionWithProduct>
{
    public IQueryable<Models.ShoppingList> Apply(DbSet<Models.ShoppingList> storage, Which.ContainsPositionWithProduct criteria)
    {
        ArgumentNullException.ThrowIfNull(storage);
        ArgumentNullException.ThrowIfNull(criteria);

        return storage.Where(sl => sl.Positions.Any(p => p.ProductId == criteria.ProductId));
    }
}