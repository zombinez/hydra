﻿using Hydra.Examples.ShoppingList.API.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Hydra.Examples.ShoppingList.API.Persistence.TypeConfigurations;

public class CustomerTypeConfiguration : IEntityTypeConfiguration<Customer>
{
    public void Configure(EntityTypeBuilder<Customer> builder)
    {
        ArgumentNullException.ThrowIfNull(builder);
        builder.HasKey(c => c.Id);
        builder.HasMany<Models.ShoppingList>().WithOne().HasForeignKey(sl => sl.CustomerId);
    }
}