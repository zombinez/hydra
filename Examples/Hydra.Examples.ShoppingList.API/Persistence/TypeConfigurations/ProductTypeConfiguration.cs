﻿using Hydra.Examples.ShoppingList.API.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Hydra.Examples.ShoppingList.API.Persistence.TypeConfigurations;

public class ProductTypeConfiguration : IEntityTypeConfiguration<Product>
{
    public void Configure(EntityTypeBuilder<Product> builder)
    {
        ArgumentNullException.ThrowIfNull(builder);

        builder.HasKey(p => p.Id);
        builder.HasMany<ShoppingListPosition>().WithOne(slp => slp.Product).HasForeignKey(slp => slp.ProductId);
    }
}