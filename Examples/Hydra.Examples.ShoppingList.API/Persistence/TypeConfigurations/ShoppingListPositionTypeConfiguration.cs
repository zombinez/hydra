﻿using Hydra.Examples.ShoppingList.API.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Hydra.Examples.ShoppingList.API.Persistence.TypeConfigurations;

public class ShoppingListPositionTypeConfiguration : IEntityTypeConfiguration<ShoppingListPosition>
{
    public void Configure(EntityTypeBuilder<ShoppingListPosition> builder)
    {
        ArgumentNullException.ThrowIfNull(builder);

        builder.HasKey(p => new { p.ShoppingListId, p.ProductId });
        builder.HasOne(p => p.Product).WithMany().HasForeignKey(p => p.ProductId);
        builder.HasOne<Models.ShoppingList>().WithMany(sl => sl.Positions).HasForeignKey(p => p.ShoppingListId);
        builder.Navigation(p => p.Product).AutoInclude();
    }
}