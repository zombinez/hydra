﻿using Hydra.Examples.ShoppingList.API.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Hydra.Examples.ShoppingList.API.Persistence.TypeConfigurations;

public class ShoppingListTypeConfiguration : IEntityTypeConfiguration<Models.ShoppingList>
{
    public void Configure(EntityTypeBuilder<Models.ShoppingList> builder)
    {
        ArgumentNullException.ThrowIfNull(builder);
        builder.HasKey(sl => sl.Id);
        builder.HasOne<Customer>().WithMany().HasForeignKey(sl => sl.CustomerId);
        builder.HasMany(sl => sl.Positions).WithOne().HasForeignKey(p => p.ShoppingListId);
        builder.Navigation(sl => sl.Positions).AutoInclude();
    }
}