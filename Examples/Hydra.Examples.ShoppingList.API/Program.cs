using Hydra.DependencyInjection.Extensions;
using Hydra.EntityFramework.DependencyInjection.Extensions;
using Hydra.Examples.ShoppingList.API.Models;
using Hydra.Examples.ShoppingList.API.Persistence;
using Hydra.Examples.ShoppingList.API.Persistence.Specifications;
using Hydra.Examples.ShoppingList.API.Persistence.Specifications.Customers;
using Hydra.Examples.ShoppingList.API.Persistence.Specifications.ShoppingList;
using Hydra.Extensions.Identity.EntityFramework.DependencyInjection.Extensions;
using Hydra.Extensions.Pagination.EntityFramework.DependencyInjection.Extensions;
using Hydra.Extensions.Pagination.SequenceProvider;
using Hydra.Extensions.Pagination.SequenceProvider.Extensions;
using Microsoft.EntityFrameworkCore;
using ShoppingLists = Hydra.Examples.ShoppingList.API.Criterias.ShoppingList;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddDbContext<AppDbContext>(o => o.UseInMemoryDatabase("shopping_list_api"));
builder.Services.AddHydra(c =>
{
    c.Resource<Customer>()
        .IsManagedByEntityFramework(
            sp => sp.GetRequiredService<AppDbContext>().Customers,
            f =>
            {
                f.Create();
                f.SearchBy(new LoginAndPasswordSpecification());
                f.SearchBy(new LoginSpecification());
                f.SearchById<Customer, Guid>();
            });
    c.Resource<ShoppingList>()
        .IsManagedByEntityFramework(
            sp => sp.GetRequiredService<AppDbContext>().ShoppingLists,
            f =>
            {
                f.Create();
                f.Modify();
                f.SearchById<ShoppingList, Guid>();
                f.SearchInPagesBy(new CustomerIdSpecification());
                f.SearchInPagesBy(new ProductInPositionSpecification())
                    .Extended((_, ps) =>
                        ps.AsSearchInSequence(
                            new StaticPagination<ShoppingList, ShoppingLists.Which.ContainsPositionWithProduct>(5)));
                f.Remove();
            });
    c.Resource<Product>()
        .IsManagedByEntityFramework(
            sp => sp.GetRequiredService<AppDbContext>().Products,
            f =>
            {
                f.Create();
                f.Modify();
                f.Remove();
                f.SearchInPagesBy(new AllSpecification<Product>());
                f.SearchById<Product, Guid>();
                f.SearchInBatchesByIds<Product, Guid>();
            });
});
builder.Services.AddScoped<ITransaction, DbContextTransaction<AppDbContext>>();
builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();