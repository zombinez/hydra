﻿namespace Hydra.Abstractions;

/// <summary>Represents component for saving newly created resource.</summary>
/// <typeparam name="TResource">The type of the resource to be saved.</typeparam>
public interface ICreator<in TResource>
{
    /// <summary>Saves newly created resource.</summary>
    /// <param name="resource">The resource to be saved.</param>
    /// <exception cref="ArgumentNullException"><paramref name="resource"/> is <see langword="null"/>.</exception>
    /// <returns>The task object representing the asynchronous operation.</returns>
    Task CreateAsync(TResource resource);
}