﻿namespace Hydra.Abstractions;

/// <summary>Represents component for saving modified resource.</summary>
/// <typeparam name="TResource">The type of the resource to be modified.</typeparam>
public interface IModifier<in TResource>
{
    /// <summary>Saves modified resource resource.</summary>
    /// <param name="resource">The resource to be saved.</param>
    /// <exception cref="ArgumentNullException"><paramref name="resource"/> instance is <see langword="null"/>.</exception>
    /// <returns>The task object representing the asynchronous operation.</returns>
    Task ModifyAsync(TResource resource);
}