﻿namespace Hydra.Abstractions;

/// <summary>Represents component for removing resource.</summary>
/// <typeparam name="TResource">The type of resource to be removed.</typeparam>
public interface IRemover<in TResource>
{
    /// <summary>Removes resource.</summary>
    /// <param name="resource">The resource to be removed.</param>
    /// <exception cref="ArgumentNullException"><paramref name="resource"/> instance is <see langword="null"/>.</exception>
    /// <returns>The task object representing the asynchronous operation.</returns>
    Task RemoveAsync(TResource resource);
}