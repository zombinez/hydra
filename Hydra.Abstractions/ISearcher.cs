﻿namespace Hydra.Abstractions;

/// <summary>Represents component for searching resource matching specified criteria.</summary>
/// <typeparam name="TResource">The type of the resource to be found.</typeparam>
/// <typeparam name="TCriteria">The type of criteria for resource to be found by.</typeparam>
public interface ISearcher<TResource, in TCriteria>
{
    /// <summary>Returns resource that matches given criteria.</summary>
    /// <param name="criteria">The criteria to search resource by.</param>
    /// <param name="cancellationToken">A <see cref="CancellationToken"/> to observe while waiting for the task to complete.</param>
    /// <exception cref="ArgumentNullException"><paramref name="criteria"/> is <see langword="null"/>.</exception>
    /// <returns>The task object representing the asynchronous operation.</returns>
    Task<TResource> SearchAsync(TCriteria criteria, CancellationToken cancellationToken = default);
}