﻿namespace Hydra.Abstractions;

/// <summary>Represents a provider for a sequence of resources matching specified criteria.</summary>
/// <typeparam name="TResource">The type of resources in sequence.</typeparam>
/// <typeparam name="TCriteria">The type of criteria to select resources by.</typeparam>
public interface ISequenceProvider<out TResource, in TCriteria>
{
    /// <summary>Gets sequence of resources matching specified criteria.</summary>
    /// <param name="criteria">The criteria to select resources by.</param>
    /// <exception cref="ArgumentNullException"><paramref name="criteria"/> is <see langword="null"/>.</exception>
    /// <returns>Sequence of resources matching certain criteria.</returns>
    IAsyncEnumerable<TResource> Get(TCriteria criteria);
}