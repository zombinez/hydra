﻿using Microsoft.Extensions.DependencyInjection;

namespace Hydra.DependencyInjection;

/// <summary>Represents a component for extending <typeparamref name="TComponent"/> as other interface.</summary>
/// <typeparam name="TComponent">The type of component to extend.</typeparam>
public class ExtensionBuilder<TComponent> where TComponent : class
{
    private readonly IServiceCollection _services;
    
    /// <summary>Initializes a new instance of <see cref="ExtensionBuilder{TCompoent}"/> with specified <see cref="IServiceCollection"/>.</summary>
    /// <param name="services">The <see cref="IServiceCollection"/> for extension components to be added in.</param>
    /// <exception cref="ArgumentNullException"><paramref name="services"/> is <see langword="null"/>.</exception>
    public ExtensionBuilder(IServiceCollection services)
    {
        _services = services ?? throw new ArgumentNullException(nameof(services));
    }

    /// <summary>Registers component of type <typeparamref name="TService"/> based on <typeparamref name="TComponent"/> and services from <see cref="IServiceProvider"/> in <see cref="IServiceCollection"/>.</summary>
    /// <param name="extendComponent">The delegate for converting component of type <see cref="TComponent"/> to component of type <see cref="TService"/>.</param>
    /// <typeparam name="TService">The type of extended component to be registered in <see cref="IServiceCollection"/>.</typeparam>
    /// <returns>The component for extending <typeparamref name="TComponent"/> as other component of type <typeparamref name="TService"/>.</returns>
    public ExtensionBuilder<TComponent> Extended<TService>(Func<IServiceProvider, TComponent, TService> extendComponent) 
        where TService : class
    {
        ArgumentNullException.ThrowIfNull(extendComponent);
        
        _services.AddScoped<TService>(sp => extendComponent(sp, sp.GetRequiredService<TComponent>()));

        return this;
    }

    /// <summary>Registers component of type <typeparamref name="TService"/> based on <typeparamref name="TComponent"/> and services from <see cref="IServiceProvider"/> in <see cref="IServiceCollection"/>.</summary>
    /// <typeparam name="TService">The type of extended component to be registered in <see cref="IServiceCollection"/>.</typeparam>
    /// <typeparam name="TImplementation">The type of implementation of <typeparamref name="TService"/> to register in <see cref="IServiceCollection"/>.</typeparam>
    /// <returns>The component for extending <typeparamref name="TComponent"/> as other component of type <typeparamref name="TService"/>.</returns>
    public ExtensionBuilder<TComponent> ExtendedAs<TService, TImplementation>()
        where TService : class 
        where TImplementation : class, TService
    {
        _services.AddScoped<TService, TImplementation>();

        return this;
    }
}