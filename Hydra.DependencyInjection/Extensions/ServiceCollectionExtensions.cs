﻿using Microsoft.Extensions.DependencyInjection;

namespace Hydra.DependencyInjection.Extensions;

public static class ServiceCollectionExtensions
{
    /// <summary>Adds Hydra component to specified <see cref="IServiceCollection"/> based on given configuration delegate.</summary>
    /// <param name="services">The <see cref="IServiceCollection"/> for Hydra components to be added in.</param>
    /// <param name="configure">The delegate for Hydra configuring.</param>
    /// <returns>The same <see cref="IServiceCollection"/> object as was given in <paramref name="services"/> parameter.</returns>
    /// <exception cref="ArgumentNullException"><paramref name="services"/> or <paramref name="configure"/> is <see langword="null"/>.</exception>
    public static IServiceCollection AddHydra(this IServiceCollection services, Action<HydraBuilder> configure)
    {
        ArgumentNullException.ThrowIfNull(services);
        ArgumentNullException.ThrowIfNull(configure);
        
        var hydraConfigurator = new HydraBuilder(services);
        configure.Invoke(hydraConfigurator);
        return services;
    }
}