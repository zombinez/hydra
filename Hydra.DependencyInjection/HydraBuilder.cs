﻿using Microsoft.Extensions.DependencyInjection;

namespace Hydra.DependencyInjection;

/// <summary>Represents a component for configuring Hydra resource management.</summary>
public class HydraBuilder
{
    private readonly IServiceCollection _services;

    /// <summary>Initializes a new instance of <see cref="HydraBuilder"/> with specified <see cref="IServiceCollection"/>.</summary>
    /// <param name="services">The <see cref="IServiceCollection" /> for Hydra components to be added in.</param>
    /// <exception cref="ArgumentNullException"><paramref name="services" /> is <see langword="null"/>.</exception>
    public HydraBuilder(IServiceCollection services)
    {
        _services = services ?? throw new ArgumentNullException(nameof(services));
    }

    /// <summary>Initializes management setup for resource of type <typeparamref name="TResource" />.</summary>
    /// <typeparam name="TResource">The type of resource to initialize management setup for.</typeparam>
    /// <returns>The <see cref="ResourceManagementSetup{TResource}" /> object for configuring resource management.</returns>
    public ResourceManagementSetup<TResource> Resource<TResource>()
    {
        return new ResourceManagementSetup<TResource>(_services);
    }
}