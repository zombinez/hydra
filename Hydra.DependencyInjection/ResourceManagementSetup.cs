﻿using Microsoft.Extensions.DependencyInjection;

namespace Hydra.DependencyInjection;

/// <summary>Represents a component for configuring management for resource of type <typeparamref name="TResource"/>.</summary>
/// <typeparam name="TResource">The type of resource for management to be configured.</typeparam>
public class ResourceManagementSetup<TResource>
{
    private readonly IServiceCollection _services;
    
    /// <summary>Initializes a new instance of <see cref="ResourceManagementSetup{TResource}"/> with specified <see cref="IServiceCollection" />.</summary>
    /// <param name="services">The <see cref="IServiceCollection" /> for Hydra components to be added in.</param>
    /// <exception cref="ArgumentNullException"><paramref name="services" /> is <see langword="null" />.</exception>
    public ResourceManagementSetup(IServiceCollection services)
    {
        _services = services ?? throw new ArgumentNullException(nameof(services));
    }

    /// <summary>Defines a <typeparamref name="TResourceManagementFeature" /> for resource to be managed with.</summary>
    /// <param name="initializeFeature">The delegate for feature initialization.</param>
    /// <param name="configureFeature">The delegate for feature configuring.</param>
    /// <typeparam name="TResourceManagementFeature">The type of feature to be used for resource.</typeparam>
    /// <exception cref="ArgumentNullException"><paramref name="initializeFeature" /> or <paramref name="configureFeature" /> is <see langword="null" />.</exception>
    /// <returns>The <see cref="ResourceManagementSetup{TResource}" /> object for configuring resource management.</returns>
    public ResourceManagementSetup<TResource> IsManagedBy<TResourceManagementFeature>(
        Func<IServiceCollection, TResourceManagementFeature> initializeFeature,
        Action<TResourceManagementFeature> configureFeature)
    {
        ArgumentNullException.ThrowIfNull(initializeFeature);
        ArgumentNullException.ThrowIfNull(configureFeature);

        var feature = initializeFeature(_services);
        configureFeature.Invoke(feature);

        return this;
    }
}