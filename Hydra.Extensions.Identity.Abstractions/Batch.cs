﻿using System.Collections;
using System.Diagnostics.CodeAnalysis;

namespace Hydra.Extensions.Identity.Abstractions;

/// <summary>Represents batch of resources with ids.</summary>
/// <typeparam name="TResource">The type of resource to be stored.</typeparam>
/// <typeparam name="TId">The type of id for the resource.</typeparam>
public class Batch<TResource, TId> : IReadOnlyCollection<TResource>
    where TResource : IIdOwner<TId>
    where TId : notnull
{
    private readonly Dictionary<TId, TResource> _resourceById;

    /// <summary>Initializes a new instance of <see cref="Batch{TResource,TId}"/> with specified resources and missing ids.</summary>
    /// <param name="resources">Resources to be contained in batch.</param>
    /// <param name="missingIds">Ids of resources that have not been found.</param>
    /// <exception cref="ArgumentNullException"><paramref name="resources"/> or <paramref name="missingIds" /> is <see langword="null" />.</exception>
    public Batch(IEnumerable<TResource> resources, IEnumerable<TId> missingIds)
    {
        ArgumentNullException.ThrowIfNull(resources);
        ArgumentNullException.ThrowIfNull(missingIds);
        
        _resourceById = resources.DistinctBy(r => r.Id).ToDictionary(r => r.Id);
        MissingIds = missingIds.ToHashSet();
    }
    
    /// <summary>Returns an enumerator that iterates through the <see cref="Batch{TResource,TId}" />.</summary>
    /// <returns>A <see cref="IEnumerator{T}" /> for the <see cref="Batch{TResource,TId}" />.</returns>
    public IEnumerator<TResource> GetEnumerator()
    {
        return _resourceById.Values.GetEnumerator();
    }

    /// <summary>Returns an enumerator that iterates through the <see cref="Batch{TResource,TId}" />.</summary>
    /// <returns>A <see cref="IEnumerator{T}" /> for the <see cref="Batch{TResource,TId}" />.</returns>
    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }
    
    /// <summary>Gets the number of resources contained in the <see cref="Batch{TResource,TId}" />.</summary>
    /// <returns>The number of resources contained in the <see cref="Batch{TResource,TId}" />.</returns>
    public int Count => _resourceById.Count;
    
    /// <summary>Gets ids of resources that have not been found.</summary>
    /// <returns>Set of ids of resources that have not been found.</returns>
    public IReadOnlySet<TId> MissingIds { get; }

    /// <summary>Gets resource with specified id.</summary>
    /// <param name="id">The id of resource to get.</param>
    /// <param name="resource">When this method returns, contains the resource with the specified id, if the id is found; otherwise, the default value for the type of the <paramref name="resource" /> parameter. This parameter is passed uninitialized.</param>
    /// <exception cref="ArgumentNullException"><paramref name="id" /> is <see langword="null" />.</exception>
    /// <returns><see langword="true" /> if <see cref="Batch{TResource,TId}" /> contains resource with given id; otherwise, <see langword="false" />.</returns>
    public bool TryGet(TId id, [NotNullWhen(true)] out TResource? resource)
    {
        return _resourceById.TryGetValue(id, out resource);
    }
}