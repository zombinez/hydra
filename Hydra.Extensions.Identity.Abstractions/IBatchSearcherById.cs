﻿using Hydra.Abstractions;

namespace Hydra.Extensions.Identity.Abstractions;

/// <summary>Represents a component for finding batch of resources with ids from specified set.</summary>
/// <typeparam name="TResource">The type of resource to be found. Must implement <see cref="IIdOwner{TId}"/>.</typeparam>
/// <typeparam name="TId">The type of id of the resource.</typeparam>
public interface IBatchSearcherById<TResource, TId> : ISearcher<Batch<TResource, TId>, IReadOnlySet<TId>>
    where TResource : IIdOwner<TId>
    where TId : notnull;