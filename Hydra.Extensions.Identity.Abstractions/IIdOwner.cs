﻿namespace Hydra.Extensions.Identity.Abstractions;

/// <summary>Represents the resource with an id.</summary>
/// <typeparam name="TId">The type of and id of the resource.</typeparam>
public interface IIdOwner<out TId> where TId : notnull
{
    /// <summary>Gets the id of the resource.</summary>
    /// <returns>An object that represents the id of the resource.</returns>
    TId Id { get; }
}