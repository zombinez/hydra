﻿using Hydra.Abstractions;

namespace Hydra.Extensions.Identity.Abstractions;

/// <summary>Represents a component for finding resource with specified id.</summary>
/// <typeparam name="TResource">The type of resource to be found. Must implement <see cref="IIdOwner{TId}"/>.</typeparam>
/// <typeparam name="TId">The type of id of the resource.</typeparam>
public interface ISearcherById<TResource, in TId> : ISearcher<TResource?, TId> 
    where TResource : IIdOwner<TId>
    where TId : notnull;