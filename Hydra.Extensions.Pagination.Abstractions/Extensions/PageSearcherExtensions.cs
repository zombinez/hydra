﻿namespace Hydra.Extensions.Pagination.Abstractions.Extensions;

public static class PageSearcherExtensions
{
    public static Task<Page<TResource>> SearchAsync<TResource, TCriteria>(
        this IPageSearcher<TResource, TCriteria> searcher, TCriteria criteria, int pageCapacity = 0, int pageOffset = 0,
        bool isWithTotalCount = false)
    {
        ArgumentNullException.ThrowIfNull(searcher);

        return searcher.SearchAsync(new PageSpecification<TCriteria>
        {
            Criteria = criteria,
            Capacity = pageCapacity,
            Offset = pageOffset,
            IsWithTotalCount = isWithTotalCount
        });
    }
}