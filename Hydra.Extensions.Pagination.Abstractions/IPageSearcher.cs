﻿using Hydra.Abstractions;

namespace Hydra.Extensions.Pagination.Abstractions;

/// <summary>Represents component for finding selection of resources matching certain criteria by pages in storage.</summary>
/// <typeparam name="TResource">The type of the resource to be found.</typeparam>
/// <typeparam name="TCriteria">The type of criteria for resource to be found by.</typeparam>
public interface IPageSearcher<TResource, TCriteria> : ISearcher<Page<TResource>, PageSpecification<TCriteria>>;