﻿using System.Collections;

namespace Hydra.Extensions.Pagination.Abstractions;

/// <summary>Represents a single page of the resource selection.</summary>
/// <typeparam name="TResource">The type of resource in page.</typeparam>
public class Page<TResource> : IReadOnlyCollection<TResource>
{
    private readonly LinkedList<TResource> _resources;
    private readonly int? _totalCount;

    /// <summary>Initializes a new instance of <see cref="Page{TResource}"/> with specified resources and missing ids.</summary>
    /// <param name="resources">Resources to be contained in page.</param>
    /// <exception cref="ArgumentNullException"><paramref name="resources"/> is <see langword="null"/>.</exception>
    public Page(IEnumerable<TResource> resources)
    {
        ArgumentNullException.ThrowIfNull(resources);
        
        _resources = new LinkedList<TResource>(resources);
    }
    
    /// <summary>Returns an enumerator that iterates through the <see cref="Page{TResource}"/>.</summary>
    /// <returns>A <see cref="IEnumerator{T}"/> for the <see cref="Page{TResource}"/>.</returns>
    public IEnumerator<TResource> GetEnumerator()
    {
        return _resources.GetEnumerator();
    }

    /// <summary>Returns an enumerator that iterates through the <see cref="Page{TResource}"/>.</summary>
    /// <returns>A <see cref="IEnumerator{T}"/> for the <see cref="Page{TResource}"/>.</returns>
    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }

    /// <summary>Gets the number of resources contained in the <see cref="Page{TResource}"/>/>.</summary>
    /// <returns>The number of resources contained in the <see cref="Page{TResource}"/>.</returns>
    public int Count => _resources.Count;

    /// <summary>Gets or sets number of resources in selection in total.</summary>
    /// <exception cref="ArgumentOutOfRangeException">Value is less than 0.</exception>
    /// <returns>The total count of resources in selection in total.</returns>
    public int? TotalCount
    {
        get => _totalCount;
        
        init
        {
            if (value < 0)
                throw new ArgumentOutOfRangeException(nameof(TotalCount), value,
                    "Total count of resources must be greater than or equal to 0.");

            _totalCount = value;
        }
    }
}