﻿namespace Hydra.Extensions.Pagination.Abstractions;

/// <summary>Represents the specification for resource selection page to be formed by.</summary>
/// <typeparam name="TCriteria">The type of criteria for resource selection to be formed by.</typeparam>
public record PageSpecification<TCriteria>
{
    private readonly int _capacity;
    private readonly int _offset;
    private readonly TCriteria? _criteria;

    /// <summary>Get or sets criteria for resource selection to be formed by.</summary>
    /// <exception cref="ArgumentNullException">Value is <see langword="null"/>.</exception>
    /// <returns>Criteria for resource selection to be formed by.</returns>
    public required TCriteria Criteria
    {
        get => _criteria!;
        
        init
        {
            if (value is null)
                throw new ArgumentNullException(nameof(Criteria));

            _criteria = value;
        }
    }

    /// <summary>Gets or sets strict maximum number of resources to be contained in selection page.</summary>
    /// <exception cref="ArgumentOutOfRangeException">Value is less than 0.</exception>
    /// <returns>Maximum number of resources in selection page.</returns>
    public int Capacity
    {
        get => _capacity;
        
        init
        {
            if (value < 0)
                throw new ArgumentOutOfRangeException(nameof(Capacity), value,
                    "Page capacity must be greater than or equal to 0.");

            _capacity = value;
        }
    }

    /// <summary>Gets or sets number of items to skip from start before forming resource selection page.</summary>
    /// <exception cref="ArgumentOutOfRangeException">Value is less than 0.</exception>
    /// <returns>Number of items to skip from start before forming resource selection page.</returns>
    public int Offset
    {
        get => _offset;
        
        init
        {
            if (value < 0)
                throw new ArgumentOutOfRangeException(nameof(Offset), value,
                    "Page offset must be greater than or equal to 0.");

            _offset = value;
        }
    }
    
    /// <summary>Gets or sets logical value defining that page should contain number of matching items in resource selection in total.</summary>
    /// <returns><see langword="true"/>, if page needs to contain number of resources matching specified criteria in total; otherwise - <see langword="false"/>.</returns>
    public bool IsWithTotalCount { get; init; }
}