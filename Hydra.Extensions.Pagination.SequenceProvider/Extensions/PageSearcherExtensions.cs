﻿using Hydra.Abstractions;
using Hydra.Extensions.Pagination.Abstractions;

namespace Hydra.Extensions.Pagination.SequenceProvider.Extensions;

public static class PageSearcherExtensions
{
    /// <summary>Converts <see cref="IPageSearcher{TResource,TCriteria}"/> to <see cref="ISequenceProvider{TResource,TCriteria}"/> with specified pagination.</summary>
    /// <param name="searcher">The resource page searcher to be converted.</param>
    /// <param name="pagination">The pagination to be used.</param>
    /// <typeparam name="TResource">The resource type to be searched.</typeparam>
    /// <typeparam name="TCriteria">The criteria for the resource to be searched by.</typeparam>
    /// <exception cref="ArgumentNullException"><paramref name="searcher"/> or <paramref name="pagination"/> is <see langword="null"/>.</exception>
    /// <returns>A <see cref="IPagination{TResource,TCriteria}"/> and <see cref="IPageSearcher{TResource,TCriteria}"/> based <see cref="ISequenceProvider{TResource,TCriteria}"/> object.</returns>
    public static ISequenceProvider<TResource, TCriteria> AsSearchInSequence<TResource, TCriteria>(
        this IPageSearcher<TResource, TCriteria> searcher, IPagination<TResource, TCriteria> pagination)
    {
        ArgumentNullException.ThrowIfNull(searcher);
        ArgumentNullException.ThrowIfNull(pagination);
        
        return new PaginationBasedSequenceProvider<TResource, TCriteria>(searcher, pagination);
    }
}