﻿using System.Diagnostics.CodeAnalysis;
using Hydra.Extensions.Pagination.Abstractions;

namespace Hydra.Extensions.Pagination.SequenceProvider;

/// <summary>Represents a component for iterating through resource sequence by pages.</summary>
/// <typeparam name="TResource">Th type of resource in sequence to be split in pages.</typeparam>
/// <typeparam name="TCriteria">The type of criteria for resource sequence to be formed by.</typeparam>
public interface IPagination<TResource, TCriteria>
{
    /// <summary>Gets or sets logical value defining that pagination has been started.</summary>
    /// <returns><see langword="true"/>, if pagination has been started; otherwise - <see langword="false"/>.</returns>
    public bool IsStarted { get; }
    
    /// <summary>Starts or resets pagination of resource selection.</summary>
    /// <param name="criteria">The criteria to be used in <see cref="PageSpecification{TCriteria}"/>.</param>
    /// <param name="firstPageSpecification">When method returns, contains specification for the first page of sequence of resources matching specified criteria.</param>
    void StartOrReset(TCriteria criteria, out PageSpecification<TCriteria> firstPageSpecification);

    /// <summary>Tries to form specification for the next page of resource selection.</summary>
    /// <param name="currentPage">The current page of resource selection.</param>
    /// <param name="nextPageSpecification">When this method returns, contains specification for the next page of resource selection, if it is possible to form the specification for the next page; otherwise, the default value for the type of <paramref name="nextPageSpecification"/>.</param>
    /// <returns><see langword="true"/> if it's possible to form the specification for the next page; otherwise, <see langword="false"/>.</returns>
    /// <exception cref="ArgumentNullException"><paramref name="currentPage"/> is <see langword="null"/>.</exception>
    /// <exception cref="InvalidOperationException">Pagination has not been started yet.</exception>
    bool TryContinue(Page<TResource> currentPage, [NotNullWhen(true)] out PageSpecification<TCriteria>? nextPageSpecification);
}