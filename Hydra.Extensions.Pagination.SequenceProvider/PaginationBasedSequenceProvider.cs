﻿using Hydra.Abstractions;
using Hydra.Extensions.Pagination.Abstractions;

namespace Hydra.Extensions.Pagination.SequenceProvider;

/// <summary>Represents a provider based on pagination for an async selection of resources matching certain criteria from storage.</summary>
/// <typeparam name="TResource">The type of resources in selection.</typeparam>
/// <typeparam name="TCriteria">The type of criteria to select resources by.</typeparam>
public class PaginationBasedSequenceProvider<TResource, TCriteria> : ISequenceProvider<TResource, TCriteria>
{
    private readonly IPageSearcher<TResource, TCriteria> _pageSearcher;
    private readonly IPagination<TResource, TCriteria> _pagination;

    /// <summary>Initializes an instance of <see cref="PaginationBasedSequenceProvider{TResource,TCriteria}"/> based on specified <see cref="IPageSearcher{TResource,TCriteria}"/> and <see cref="IPagination{TResource,TCriteria}"/>.</summary>
    /// <param name="pageSearcher">The <see cref="IPageSearcher{TResource,TCriteria}"/>> for provider to be based on.</param>
    /// <param name="pagination">The <see cref="IPagination{TResource,TCriteria}"/> for provider to be based on.</param>
    /// <exception cref="ArgumentNullException"><paramref name="pageSearcher"/> or <paramref name="pagination"/> is <see langword="null"/>.</exception>
    public PaginationBasedSequenceProvider(IPageSearcher<TResource, TCriteria> pageSearcher, IPagination<TResource, TCriteria> pagination)
    {
        _pageSearcher = pageSearcher ?? throw new ArgumentNullException(nameof(pageSearcher));
        _pagination = pagination ?? throw  new ArgumentNullException(nameof(pagination));
    }

    /// <summary>Gets sequence of resources matching specified criteria.</summary>
    /// <param name="criteria">The criteria for resource selection to be formed by.</param>
    /// <returns>The sequence of resources matching specified criteria.</returns>
    public async IAsyncEnumerable<TResource> Get(TCriteria criteria)
    {
        Page<TResource> page;
        _pagination.StartOrReset(criteria, out var pageSpecification);

        do
        {
            page = await _pageSearcher.SearchAsync(pageSpecification);
            foreach (var resource in page)
                yield return resource;
        } 
        while (_pagination.TryContinue(page, out pageSpecification));
    }
}