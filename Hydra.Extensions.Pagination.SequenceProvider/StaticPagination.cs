﻿using System.Diagnostics.CodeAnalysis;
using Hydra.Extensions.Pagination.Abstractions;

namespace Hydra.Extensions.Pagination.SequenceProvider;

/// <summary>Represents a component for iterating through resource sequence by pages with constant capacity.</summary>
/// <typeparam name="TResource">The type of resource in sequence to be split in pages.</typeparam>
/// <typeparam name="TCriteria">The type of criteria for resource sequence to be formed by.</typeparam>
public class StaticPagination<TResource, TCriteria> : IPagination<TResource, TCriteria>
{
    private readonly int _pageCapacity;
    private TCriteria? _criteria;
    private int? _totalResourcesCount;
    private int _receivedResourcesCount;

    /// <summary>Initializes a new instance of <see cref="StaticPagination{TResource,TCriteria}"/> with specified page capacity.</summary>
    /// <param name="pageCapacity">The page capacity.</param>
    public StaticPagination(int pageCapacity)
    {
        _pageCapacity = pageCapacity;
    }

    /// <summary>Gets or sets logical value defining that pagination has been started.</summary>
    /// <returns><see langword="true"/>, if pagination has been started; otherwise - <see langword="false"/>.</returns>
    public bool IsStarted => _criteria is not null;

    /// <summary>Starts or resets pagination of resource selection.</summary>
    /// <param name="criteria">The criteria to be used in <see cref="PageSpecification{TCriteria}"/>.</param>
    /// <param name="firstPageSpecification">When method returns, contains specification for the first page of sequence of resources matching specified criteria.</param>
    public void StartOrReset(TCriteria criteria, out PageSpecification<TCriteria> firstPageSpecification)
    {
        _criteria = criteria;

        firstPageSpecification = new PageSpecification<TCriteria>
        {
            Criteria = criteria,
            Offset = 0,
            Capacity = _pageCapacity,
            IsWithTotalCount = true
        };
    }

    /// <summary>Tries to form specification for the next page of resource selection.</summary>
    /// <param name="currentPage">The current page of resource selection.</param>
    /// <param name="nextPageSpecification">When this method returns, contains specification for the next page of resource selection, if it is possible to form the specification for the next page; otherwise, the default value for the type of <paramref name="nextPageSpecification"/>.</param>
    /// <returns><see langword="true" /> if it's possible to form the specification for the next page; otherwise, <see langword="false" />.</returns>
    /// <exception cref="ArgumentNullException"><paramref name="currentPage"/> is <see langword="null"/>.</exception>
    /// <exception cref="InvalidOperationException">Pagination has not been started yet.</exception>
    public bool TryContinue(Page<TResource> currentPage, [NotNullWhen(true)] out PageSpecification<TCriteria>? nextPageSpecification)
    {
        if (!IsStarted)
            throw new InvalidOperationException("Pagination cannot continue if it has not been started yet.");
        
        ArgumentNullException.ThrowIfNull(currentPage);
        nextPageSpecification = null;
        
        if (currentPage.Count == 0)
            return false;
        
        if (currentPage.TotalCount is not null && (_totalResourcesCount is null || CheckIfTotalCountWasChanged(currentPage.TotalCount.Value)))
        {
            _totalResourcesCount = currentPage.TotalCount;
        }

        if (CheckIfAllItemsReceived(currentPage.Count))
            return false;

        _receivedResourcesCount += currentPage.Count;

        nextPageSpecification = new PageSpecification<TCriteria>
        {
            Criteria = _criteria!,
            Capacity = GetNextPageCapacity(),
            Offset = _receivedResourcesCount
        };
        return true;
    }

    private bool CheckIfAllItemsReceived(int currentReceivedResourcesCount)
    {
        return currentReceivedResourcesCount + _receivedResourcesCount >= _totalResourcesCount;
    }

    private bool CheckIfTotalCountWasChanged(int totalCount)
    {
        return totalCount != _totalResourcesCount;
    }

    private int GetNextPageCapacity()
    {
        if (_totalResourcesCount == null) 
            return _pageCapacity;
        
        var remainingResourcesCount = _totalResourcesCount.Value - _receivedResourcesCount;
        return _pageCapacity >= remainingResourcesCount ? remainingResourcesCount : _pageCapacity;
    }
}